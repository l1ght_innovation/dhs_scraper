import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir)
import database
import re
import pprint
from collections import Counter
import datetime
import extract
import traceback,sys



def get_texts_from_log(conn):
    cursor=conn.cursor()
    sql="select source,text from log"
    cursor.execute(sql)
    results=cursor.fetchall()
    return results


def find_match(conn,texts):
    cursor=conn.cursor()
    matches=0
    count=0
    total=len(texts)
    for text in texts:
        count+=1
        print(count,'/',total)
        text=text['text']
        sql="select text from chats where text = %s "
        val=(text)
        cursor.execute(sql,val)
        results=cursor.fetchall()
        if len(results)>0:
            matches+=1
            print(matches,'found match: ',text)
    return

def match_by_time(conn,texts):
    cursor=conn.cursor()
    count=Counter()
    miss_dict={}
    for text in texts:
        regex="\s*[0-9][0-9]-[0-9][0-9]\s[0-9][0-9]:[0-9][0-9]:[0-9][0-9]"
        time = re.compile(regex)
        a=time.search(text['text'])
        if not a:
            #print(a,time, text)
            count[text['source']]+=1
        else:
            try:
                #print(text, a.group(0))
                date_str=a.group(0).strip()
                date_obj=datetime.datetime.strptime('2020-'+date_str, "%Y-%m-%d %H:%M:%S")
                #print(date_obj)
                sql="select * from chats where date = %s "
                vals=(date_obj)
                cursor.execute(sql,vals)
                results=cursor.fetchall()
                # print(results)
                if len(results) > 0:
                    for res in results:
                        print(text,res['text'],res['date'])
            except Exception as e:
                print('oops',e)
                continue
    pprint.pprint(count)
    return


def create_features(conn):
    cursor=conn.cursor()
    sql="select * from log"
    cursor.execute(sql)
    results=cursor.fetchall()
    regex = '[0-9][0-9]-[0-9][0-9]\s[0-9][0-9]:[0-9][0-9]:[0-9][0-9]'
    time = re.compile(regex)
    for res in results:
        try:
            text=res['text']
            res['full_text']=text
            if text.strip().startswith('DEL'):
                text=text[len('DEL'):].strip()
            start_index,features=get_text_start_index(text)
            res['text']=text[text.find('-',start_index)+1:].strip()
            res['features']=','.join(features)
            print(text,start_index)
            a=time.search(text)
            date_str = a.group(0).strip()
            date_obj = datetime.datetime.strptime('2020-' + date_str, "%Y-%m-%d %H:%M:%S")
            res['date']=date_obj
            user_start=text.find('-',a.span()[1])+1
            user_end=text.find('-',user_start+1)
            user=text[user_start:user_end].strip()
            print(text,'user: ',user)
            if user_end == -1:
                if text.find(']')!=-1:
                    speaker=text[text.rfind(']')+1:text.find(' ',text.rfind(']')+2)].strip()
                    recipient=None
                    private_msg=False
                else:
                    speaker='system'
                    recipient=text[user_start: text.find(' ',user_start+2)].strip()
                    private_msg=False
            elif len(user.split(' '))>1 and user[0]=='[' and user[-1]==']':
                index1=user.rfind('[')
                index2=user.rfind(']')
                user_block=user[index1+1:index2]
                user_split=user_block.split(' ')
                speaker=user_split[0].strip()
                recipient=user_split[-1].strip()
                private_msg=True
            elif len(user.split(' '))>1 and user[0]=='[' and user[-1]!=']':
                private_msg=False
                speaker=user.split(' ')[-1].strip()
                recipient=None
            else:
                private_msg=False
                speaker=user
                recipient=None
            if speaker.startswith('['):
                i=text.find('-')
                i=text.find('-',i+1)
                if i!=-1:
                    open=True
                    while open and i<len(text)-1:
                        char=text[i]
                        if char=='[':
                            open=True
                        elif char==']' and open and not text[i+2] =='[':
                            open=False
                        i+=1
                    start=i
                    end=text.find('-',start)
                    print('text:',text,'start:',start,'end',end)
                    if end ==-1:
                        speaker='system'
                        recipient=text[start:text.find(' ')].strip()
                        private_msg=False
                    else:
                        speaker=text[start:end]
                        recipient=None
                        private_msg=False

            #print(text[user_start:user_end])
            id=res['id']
            res['user_name']=speaker
            res['private_msg']=private_msg
            res['recipient']=recipient
            res['site']=res['source']
            #res['id']=res['index']
            res_with_flags=extract.add_flags(res)
            insert_into_log_extraction(conn,res['site'],res_with_flags)
            # print(res_with_flags)
            # print('-------')
        except Exception as e:
            print('FAILED ON ',res,e)
            traceback.print_exc(file=sys.stdout)
            continue
    return



# def insert_into_log_extraction(conn,site,userid,user_name,date,text,private_msg,recipient):
#     cursor=conn.cursor()
#     sql="insert into log_extraction (site,userid,user_name,date,text,private_msg,recipient) values (%s,%s,%s,%s,%s,%s,%s)"
#     val=(site,userid,user_name,date,text[:500],private_msg,recipient)
#     try:
#         cursor.execute(sql,val)
#         conn.commit()
#     except Exception as e:
#         print ('could not insert line',vars())
#     return

def insert_into_log_extraction(conn,site,results):
    cursor = conn.cursor()
    sql,val=build_sql_query(conn,site,results)
    try:
        cursor.execute(sql, val)
        conn.commit()
        results['index']=cursor.lastrowid
    except Exception as e:
        print('could not insert line ', e)
    return results

def build_sql_query(conn,site,results):
    #print('results in build sql: ',results)
    date_time=results['date']
    results['image_links']=''
    results['text']=results['text'][:500]
    results['full_text']=results['full_text'][:500]
    results['userid']=''
    results['religions']=results['religions'][:50]
    results['countries']=results['countries'][:50]
    results['scrape_id']=''
    sql="insert into log_extraction (site,date"
    values=" values (%s,%s"
    vals=(site,date_time)
    for key,val in results.items():
        if key=='date' or key=='source' or key=='line_number' or key=='site' or key=='scrape_id':
            continue
        sql=sql+","+key
        values=values+',%s'
        vals+=(val,)
    sql+=")"
    values+=")"
    sql_query=sql+values
    return sql_query,vals


def get_text_start_index(text):
    # text="10-18 21:13:47 - [Pub-Staff-Meeting] [Admin] larry79 - it wouuld make one difference visible between the ranks"
    # text='aaa'
    #index for time
    regex="[0-9][0-9]-[0-9][0-9]\s[0-9][0-9]:[0-9][0-9]:[0-9][0-9]"
    reg=re.compile(regex)
    res=reg.search(text)
    if res is None:
        index=0
    else:
        index=res.span()[1]
    # print('>>>',res)
    # print('>>>',res.span()[1])
    # print('>>>',index)

    #index for square brackets
    regex="\[([^]]+)\]"
    reg=re.compile(regex)
    res=re.findall(reg,text)
    features=[]
    # print('>>>>',res)
    if len(res)>0:
        index=text.find(res[-1])+len(res[-1])
        features=res
        # print('final index: ',index)
    else:
        index=index
    #print(res)

    final_index=text.find('-',index+2)
    #print(final_index)
    if final_index==-1:
        final_index=index
    return (final_index,features)

def get_unfeatured_texts(conn):
    cursor=conn.cursor()
    sql="select * from log"
    cursor.execute(sql)
    results=cursor.fetchall()
    total=len(results)
    i=0
    texts=[]
    for result in results:
        i+=1
        print('getting text ',i,'/',total)
        sql="select * from log_extraction where id = %s"
        vals=result['id']
        cursor.execute(sql,vals)
        res=cursor.fetchall()
        if len(res)<1:
            texts.append(result['text'])
    text_for_file='\n'.join(texts)
    print('failed lines: ',len(texts))
    with open ('failed_texts.txt','w+') as f:
        f.write(text_for_file)



if __name__=="__main__":
    conn=database.connect_db()
    get_unfeatured_texts(conn)
    # create_features(conn)
    conn.close()
    # get_text_start_index('aaa')