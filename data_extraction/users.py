import os,sys,inspect
import docx
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir)
import database
import datetime
import pandas as pd


def get_user_dict_from_docx_without_table(path,date,site):
    results = []
    try:
        doc = docx.Document(path)
    except:
        doc = None
    print("doc without table: ", doc)
    if doc is not None:
        for line in doc.paragraphs:
            print(line.text)
            par_lines = line.text.split('\n')
            print(par_lines)
            for a_line in par_lines:
                line_arr = a_line.split('\t')
                if len(line_arr) == 2:
                    user = line_arr[0]
                    if user == 'Nickname':
                        continue
                    user_name = user.split(' ')[0]
                    if len(user.split(' ')) > 1:
                        ranking = user.split(' ')[1].replace('(', '').replace(')', '')
                    else:
                        ranking = 'unknown'
                    results.append({"user_name": user_name, "ranking": ranking, "date": date, "site": site})
    return results


def get_user_dict_from_docx_with_table(path,date,site):
    print('cccc',path)
    results = []
    try:
        doc = docx.Document(path)
    except:
        doc = None
    if doc is not None:
        tables=doc.tables
        print('bbb',tables)
        for table in tables:
            for row in table.rows:
                for cell in row.cells:
                    for paragraph in cell.paragraphs:
                        if paragraph.text.strip().endswith('AM') or paragraph.text.strip().endswith('PM') or paragraph.text.lower()=='nickname' or paragraph.text=='unknown':
                            continue
                        else:
                            user_block=paragraph.text
                            user_name=user_block.split(' ')[0].strip()
                            try:
                                ranking=user_block.split(' ')[1][1:-1]
                            except:
                                ranking='unknown'
                        results.append({"user_name": user_name, "ranking": ranking, "date": date, "site": site})
    return results

def read_file(file,site,date):
    user_dict=[]
    try:
        doc = docx.Document(file)
    except:
        doc = None
    print("doc: ",doc,file)
    if doc is not None:
        paragraphs=doc.paragraphs
        tables=doc.tables
        print('llll',file,len(tables))
        if len(tables)==0:
            print('going to get without table')
            user_dict=get_user_dict_from_docx_without_table(file,date,site)
            print('got ',user_dict)
        else:
            print('going to get with table')
            user_dict=get_user_dict_from_docx_with_table(file,date,site)
            print('got ',user_dict)
    return user_dict

def set_enddate_to_old_ranking(conn,id):
    cursor=conn.cursor()
    end_date=datetime.date.today()
    sql="update users set enddate=%s where id=%s"
    vals=(end_date,id)
    cursor.execute(sql,vals)
    conn.commit()
    return


def create_new_line(conn,user_dict):
    cursor=conn.cursor()
    date_str=datetime.datetime.strftime(user_dict['date'],'%m-%d-%Y %H:%M:%S')
    sql="insert into users(user_name,site,ranking,startdate) values (%s,%s,%s,%s);"
    vals=(user_dict['user_name'],user_dict['site'],user_dict['ranking'],user_dict['date'])
    cursor.execute(sql,vals)
    conn.commit()
    return

def update_user(conn,user_dict):
    cursor=conn.cursor()
    sql="select * from users where user_name = %s and site = %s "
    vals=(user_dict['user_name'],user_dict['site'])
    cursor.execute(sql,vals)
    results=cursor.fetchall()
    if len(results)==0:
        create_new_line(conn,user_dict)
    else:
        for result in results:
            if result['enddate'] is None and result['ranking']!=user_dict['ranking']:
                set_enddate_to_old_ranking(conn,result['id'])
                create_new_line(conn,user_dict)
            else:
                continue
    return


def update_users_table(conn,user_list):
    for user in user_list:
        update_user(conn,user)
    return

def get_file_date(path):
    now=datetime.datetime.now()
    return now

def get_file_site(path):
    site='unknown'
    if 'bp' in path.lower() or 'boyspub' in path.lower():
        site='BoysPub'
    elif 'lp' in path.lower() or 'lolipub' in path.lower():
        site='LoliPub'
    print('returning ',site, "for ",path)
    return site


def get_all_files(path):
    all_files=[]
    for r,d,f in os.walk(path):
        for file in f:
            file_path=os.path.join(r,file)
            all_files.append(file_path)
    print('all: ',all_files)
    return(all_files)


def main(path):
    conn=database.connect_db()
    files=get_all_files(path)
    #read file - resturn a list. for each line a dict {"user_name":user_name,"ranking":ranking,"date":date}
    for file in files:
        date=get_file_date(file)
        site=get_file_site(file)
        user_list=read_file(file,site,date)
        print('aaaa',file,user_list)
        if len(user_list)>0:
            update_users_table(conn,user_list)
    conn.close()
    return

if __name__=="__main__":
    path='data_extraction/assets/users'
    main(path)


