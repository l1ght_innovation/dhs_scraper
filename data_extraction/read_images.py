from pytesseract import image_to_string
import face_recognition
import cv2
import os
import pandas as pd
import re


def get_ocr_on_image(imagefile):
    image1 = cv2.imread(imagefile,0)
    image2 = cv2.imread(imagefile,1)
    resultsbw = image_to_string(image1, lang='eng')
    resultscolor = image_to_string(image2, lang='eng')
    if resultsbw.count(' ')>resultscolor.count(' '):
        recommendation = resultscolor
    else:
        recommendation = resultsbw
    return recommendation, resultsbw, resultscolor


def testcase():
    imagefile = '/Users/johndoe/Desktop/L1ght Test/BoysPub LoliPub screenshots/Screen Shot 2020-08-31 at 5.40.38 AM.png'
    text, text1, text2 = get_ocr_on_image(imagefile)
    return print(text, text1, text2)

def ocr_directory(path, outputfile):
    regex="[0-9][0-9]-[0-9][0-9] [0-9][0-9]:[0-9][0-9].*"
    results = []
    for dirpath, dirnames, filenames in os.walk(path):
            for file in [f for f in filenames if f.endswith('.jpg') or f.endswith('.gif') or f.endswith('.jpeg') or f.endswith('.png')]:
                filename = os.path.join(dirpath,file)
                print("Checking: ", filename)
    linesout = pd.DataFrame(results)
    linesout.to_csv(outputfile, index=False)
    return linesout


#ocr_directory('/Users/l1ght/code/DHS_scrape/data_extraction/assets', 'l1ghttest.csv')
#ocr_directory('/Users/johndoe/Desktop/L1ght Test/BoysPub LoliPub screenshots', 'boyspub.csv')