import os,sys,inspect
import read_images
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir)
import database
import extract
import pandas as pd
import pprint
import re
import docx
from bs4 import BeautifulSoup
import datetime
import pytz
import ast
import os
import compare_scrapes


utc = pytz.timezone('UTC')
allowed_extentions={
    'image':['jpg','jpeg','gif','png'],
    'text':['txt','docx'],
    'spreadsheet':['xlsx','csv'],
    'web':['html'],
}

regex="[0-9][0-9]-[0-9][0-9] [0-9][0-9]:[0-9][0-9]:[0-9][0-9].*"

def get_all_allowed_extensions(ext_dict):
    all_allowed_extensions=[]
    for key,val in ext_dict.items():
        all_allowed_extensions+=val
    return all_allowed_extensions


all_allowed_extensions=get_all_allowed_extensions(allowed_extentions)




#  id, filename, line number, text, empty/not
# file='data_extraction/assets/L1/Solis extractions/Ronja_Messages_2020-02-05 .xlsx'
# file = 'data_extraction/assets/L1/Lolipub chat/LP_200926_ut Svennis_SC.txt'

def extract_data_from_xls_file(file):
    df=pd.read_excel(file,header=None)
    conn=database.connect_db()
    source=file
    for i in range(len(df)):
        text=df.at[i,11]
        insert_line_to_db(conn,source,i,text)
    conn.close()
    return


def insert_line_to_db(conn,source,line_number,text):
    cursor=conn.cursor()
    sql="insert into log (source,line_number,text) values (%s,%s,%s)"
    vals=source,line_number,text
    cursor.execute(sql,vals)
    id=cursor.lastrowid
    conn.commit()
    return id


def insert_results_into_db(conn,results):
    for i,result in enumerate(results):
        source=result['source'].replace('data_extraction/assets','')
        line_number=result['line_number']
        text=result['text']
        id=insert_line_to_db(conn,source,line_number,text)
        results[i]["id"]=id
    return results


def extract_msgs_from_html(file):
    f=open(file,encoding='utf-8')
    soup=BeautifulSoup(f,'lxml')
    messages=soup.findAll(class_='msg')
    results=[]
    lines=[]
    for i,message in enumerate(messages):
        text=message.text.strip()
        lines.append({"line_number":i,"text":text})
        # result={"source":file,"line_number":i,"text":text}
        #print(result)
        # results.append(result)
    results={"file":file,"lines":lines,}
    return results


def get_all_files(folder):
    results=[]
    for r,d,f in os.walk(folder):
        for file in f:
            path=os.path.join(r,file)
            ext=file.split('.')[-1]
            if ext in all_allowed_extensions:
                results.append(path)
            else:
                print('no support for file type ',ext)
    return results


def get_lines_from_text_file(file,type="txt"):
    new_line_regex="[0-9][0-9]-[0-9][0-9]\s[0-9][0-9]:[0-9][0-9]:[0-9][0-9]"
    reg=re.compile(new_line_regex)
    print('getting from file: ',file,type)
    lines=[]
    if type=="txt":
        try:
            linesin=open(file).readlines()
        except:
            linesin=[]
    elif type=='docx':
        try:
            doc=docx.Document(file)
        except:
            doc=None
        linesin=[]
        if doc is not None:
            for line in doc.paragraphs:
                par_lines=line.text.split('\n')
                for a_line in par_lines:
                    linesin.append(a_line)
    else:
        print('invalid file type ',file)
    proper_lines=[]
    EOF=False
    i=0
    new_line=[]
    while not EOF:
        try:
            current_line=linesin[i]
        except:
            break
        # print(i,len(linesin),current_line)
        res1=reg.search(current_line)
        if res1 is not None:
            if len(new_line)>0:
                proper_lines.append(' '.join(new_line).replace('\n',' ').strip())
                # print('pl: ',proper_lines)
                new_line=[current_line]
            else:
                new_line=[current_line]
                # new_line.append(current_line)
        else:
            if len(new_line)>0:
                new_line.append(current_line)
            else:
                print('no line to append to')
        i+=1
        if i>len(linesin)-3:
            EOF=True
    for i,line in enumerate(proper_lines):
        if len(line)>0:
            lines.append({"line_number":i,"text":line.replace('\n','').replace('\t',' ')})
    results={"file":file,"lines":lines}
    return results


def get_lines_from_xlsx_file(file):
    df=pd.read_excel(file,header=None)
    lines=[]
    for i in range(len(df)):
        text=str(df.at[i,11])
        if len(text)>0:
            lines.append({"line_number":i,"text":text.replace('\n','').replace('\t',' ')})
    results={"file":file,"lines":lines}
    return results


def get_lines_from_csv_file(file):
    df=pd.read_csv(file)
    lines=[]
    for index,row in df.iterrows():
        lines.append({"line_number":index,"text":row['text'].replace('\n','').replace('\t',' ')})
    result={"file":file,"lines":lines}
    return result


def get_lines_from_image_file(file):
    results=[]
    lines=[]
    try:
        recommendation, textbw, textcolor = read_images.get_ocr_on_image(file)
        found = True
    except:
        recommendation = ''
        textbw = ''
        textcolor = ''
        found = False
    print(recommendation)
    chats = re.findall(regex, recommendation)
    for i,chat in enumerate(chats):
        if len(chat)>0:
            chat=chat.replace('\n',' ').replace('\t',' ')
            lines.append({"line_number":i,"text":chat})
    if len(lines)>0:
        results={"file":file,"lines":lines}
    else:
        results={}
    print(chats)
    return results


def get_lines_from_web_file(file):
    f=open(file,encoding='utf-8')
    try:
        soup=BeautifulSoup(f,'lxml')
    except:
        soup=''
    messages=soup.findAll(class_='msg')
    lines=[]
    for i,message in enumerate(messages):
        text=message.text.strip()
        if len(text)>0:
            lines.append({"line_number":i,"text":text})
    results={"file":file,"lines":lines,}
    return results


def get_all_lines(files):
    all_lines=[]
    for file in files:
        ext=file.split('.')[-1]
        if ext in allowed_extentions['text']:
            lines=get_lines_from_text_file(file,type=ext)
        elif ext in allowed_extentions['spreadsheet']:
            if ext=='xlsx':
                lines=get_lines_from_xlsx_file(file)
            elif ext=='csv':
                lines=get_lines_from_csv_file(file)
        elif ext in allowed_extentions['image']:
            lines=get_lines_from_image_file(file)
        elif ext in allowed_extentions['web']:
            lines=get_lines_from_web_file(file)
        else:
            print('cannot handle file with this extension: ',ext)
        if lines!={}:
            all_lines.append(lines)
    return all_lines


def insert_lines_into_log_table(conn,results):
    cursor=conn.cursor()
    for i,result in enumerate(results):
        source=result["file"]
        lines=result["lines"]
        for j,line in enumerate(lines):
            text=line["text"]
            line_number=line["line_number"]
            sql="insert into log (source,line_number,text) values (%s,%s,%s)"
            vals=(source,line_number,text)
            cursor.execute(sql,vals)
            conn.commit()
            id=cursor.lastrowid
            line["id"]=id
    return results


def create_feature_dict(lines_dict):
    results=[]
    for line in lines_dict:
        source=line['file']
        chats=line['lines']
        for chat in chats:
            line_features = get_line_features(chat,source)
            results.append(line_features)
    return results


def get_time(line):
    date_str=None
    date_obj=None
    regex = '[0-9][0-9]-[0-9][0-9]\s[0-9][0-9]:[0-9][0-9]:[0-9][0-9]'
    time = re.compile(regex)
    text_time=time.search(line)
    if text_time is not None:
        date_str=text_time.group(0)
        try:
            date_obj = datetime.datetime.strptime('2020-' + date_str, "%Y-%m-%d %H:%M:%S")
        except:
            date_obj=None
    return date_str,date_obj


def get_room_features(line):
    private_msg=None
    regex="\[([^]]+)\]"
    reg=re.compile(regex)
    features=re.findall(reg,line)
    if len(features)>0:
        regex = " to "
        prv_msg=re.compile(regex)
        for i,feature in enumerate(features):
            feature_pvt_msg=prv_msg.search(feature)
            if feature_pvt_msg is not None:
                private_msg=feature
                features.pop(i)
        if len(features)==0:
            features=None
        else:
            features=','.join(features)
    else:
        features=None
    return features,private_msg


def get_user(line,date_str,room_features):
    room_features,pvt_msg=get_room_features(line)
    if date_str is None:
        user_name=None
        recipient=None
        text_index=None
    elif date_str is not None and room_features is None:
        last_date_index=line.find(date_str)+len(date_str)
        first_dash_index=line.find('-',last_date_index)
        second_dash_index=line.find('-',first_dash_index)
        third_dash_index=line.find('-',second_dash_index+1)
        space_after_username_index=line.find(' ',second_dash_index+3)
        if third_dash_index==-1 or space_after_username_index ==-1:
            index=max(third_dash_index,space_after_username_index)
        else:
            index=min(third_dash_index,space_after_username_index)
        user_name=line[second_dash_index+1:index].strip()
        recipient='all'
        text_index=third_dash_index+1
    elif room_features is not None:
        room_features=room_features.split(',')
        last_room_features_index=line.find(room_features[-1])+len(room_features[-1])
        last_user_name_index1=line.find('-',last_room_features_index+2)
        last_user_name_index2=line.find(' ',last_room_features_index+2)
        if last_user_name_index1 ==-1 and last_user_name_index2 !=-1:
            user_name=line[last_room_features_index+1:last_user_name_index2].strip()
            recipient=','.join(room_features)
            text_index=last_user_name_index2
        elif last_user_name_index2==-1 and last_user_name_index1!=-1:
            user_name=line[last_room_features_index+1:last_user_name_index2].strip()
            recipient=','.join(room_features)
            text_index=last_user_name_index1
        else:
            index=min(last_user_name_index1,last_user_name_index2)
            user_name=line[last_room_features_index+1:index].strip()
            recipient=','.join(room_features)
            text_index=index
    if pvt_msg is not None:
        if room_features is None:
            user_name = pvt_msg.split(' to ')[0].strip()
            recipient=pvt_msg.split(' to ')[-1].strip()
            text_index = line.find(pvt_msg) + len(pvt_msg) + 1
        else:
            user_name = pvt_msg.split(' to ')[0].strip()
            recipient = recipient + ',' + pvt_msg.split(' to ')[-1].strip()
            text_index = line.find(pvt_msg) + len(pvt_msg) + 1
    return user_name,recipient,text_index


def split_path(path):
    allparts = []
    while 1:
        parts = os.path.split(path)
        if parts[0] == path:  # sentinel for absolute paths
            allparts.insert(0, parts[0])
            break
        elif parts[1] == path: # sentinel for relative paths
            allparts.insert(0, parts[1])
            break
        else:
            path = parts[0]
            allparts.insert(0, parts[1])
    return allparts


def get_site(source):
    source_l=source.lower()
    source_arr=split_path(source_l)
    res=None
    for i,path_part in enumerate(source_arr):
        if 'boyspub' in path_part:
            res='BoysPub'
        elif 'lolipub' in path_part:
            res='LoliPub'
    return res

def get_contacts(line):
    text=line['text']
    text_split=text.split(' ')
    contacts=[]
    for index,word in enumerate(text_split):
        if word=="@":
            try:
                word=text_split[index]+text_split[index+1]
            except:
                continue
        else:
            word=word
        print(word)
        if len(word)>0 and word[0]=="@":
            regex = re.compile('[^a-zA-Z0-9]')
            contact=regex.sub('', word)
            contacts.append(contact)
    if len(contacts)==0:
        contacts=None
    else:
        contacts=','.join(contacts)
    print('retruning these contact: ',contacts)
    return contacts


def get_line_features(line,source):
    res={}
    full_text=line['text']
    res['full_text']=full_text
    res['id']=line['id']
    res['source']=source
    res['site']=get_site(source)
    res['recipient']=None
    res['user_name']=None
    # res['chatroom']=None
    res['date']=None
    res['private_msg']=False
    date_str,date_obj=get_time(full_text)
    if date_obj is not None:
        res['date']=date_obj
    room_features,pvt_msg=get_room_features(full_text)
    # if room_features is not None:
    #     res['chatroom']=room_features
    if pvt_msg is not None:
        res['private_msg']=True
    user_name,recipient,text_index=get_user(full_text,date_str,room_features)
    res['user_name'] = user_name
    res['recipient'] = recipient
    print('text: ',full_text)
    if text_index is not None:
        print(text_index)
        res['text']=full_text[text_index:].strip()
        print('99999999',res['text'])
        if len(res['text'])>0 and res['text'][0]=='-':
            res['text']=res['text'][1:].strip()
    else:
        res['text']=full_text
    res['known_contacts']=get_contacts(line)
    print(res)
    res_with_flags=extract.add_flags(res)
    print(res_with_flags)
    return(res_with_flags)





def build_sql_query(site,results):
    #print('results in build sql: ',results)
    if results['user_name'] is not None:
        results['user_name']=results['user_name'][:50]
    date_time=results['date']
    if site:
        site=site[:100]
    results['source']=results['source'][:100]
    results['text']=results['text'][:500]
    results['full_text']=results['full_text'][:500]
    results['userid']=''
    results['religions']=results['religions'][:50]
    results['countries']=results['countries'][:50]
    results['scrape_id']=''
    sql="insert into log_extraction (site,date"
    values=" values (%s,%s"
    vals=(site,date_time)
    for key,val in results.items():
        if key=='date' or key=='site'  or key=='line_number' or key=='scrape_id':
            continue
        sql=sql+","+key
        values=values+',%s'
        vals+=(val,)
    sql+=")"
    values+=")"
    sql_query=sql+values
    return sql_query,vals

def insert_line_into_log_extraction_table(conn,result):
    cursor=conn.cursor()
    sql,val=build_sql_query(result['site'],result)
    try:
        cursor.execute(sql,val)
        conn.commit()
    except Exception as e:
        print('could not insert line ',e)

def insert_lines_into_log_extraction_table(conn,results):
    for result in results:
        insert_line_into_log_extraction_table(conn,result)
    return

def main(path):
    conn=database.connect_db()
    files=get_all_files(path)
    lines_dict=get_all_lines(files)
    lines_dict=insert_lines_into_log_table(conn,lines_dict)
    feature_dict=create_feature_dict(lines_dict)
    insert_lines_into_log_extraction_table(conn,feature_dict)
    conn.close()
    return


if __name__=="__main__":
    start=datetime.datetime.now()
    if len(sys.argv)<2:
        path="data_extraction/assets"
    else:
        path=sys.argv[1]
    print(path)
    main(path)
    end=datetime.datetime.now()
    dur=end-start
    print('finished in ',dur)