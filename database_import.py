import pandas as pd
import pymysql
import pymysql.cursors
import tldextract

rds_host = "localhost"
rds_name = 'admin'
rds_password = 'Antitoxin613'
rds_db_name = 'webscrapper'


def connect_db():
    conn = pymysql.connect(rds_host, port=3306, user=rds_name, passwd=rds_password, db=rds_db_name,cursorclass=pymysql.cursors.DictCursor)
    return conn

def insert_record(conn, row):
    cursor = conn.cursor()
    domain = tldextract.extract(row[0])
    sql = "insert into sites (url, ip, batch, file, previous, status, domainname) values (%s, %s, %s, %s, %s, %s, %s)"
    val = (row[0], row[1], row[2], row[3], row[4], row[5], domain.domain+"."+domain.suffix)
    #print("val is: ", val)
    cursor.execute(sql, val)
    conn.commit()
    return


files = ['/Users/johndoe/Documents/hezidatadump-batch1.csv', '/Users/johndoe/Documents/hezidatadump-batch2.csv', '/Users/johndoe/Documents/hezidatadump-batch3.csv', '/Users/johndoe/Documents/hezidatadump-batch4.csv', '/Users/johndoe/Documents/hezidatadump-batch51.csv', '/Users/johndoe/Documents/hezidatadump-batch52.csv', '/Users/johndoe/Documents/hezidatadump-batch53.csv', '/Users/johndoe/Documents/hezidatadump-batch54.csv']

conn = connect_db()

for file in files:
    linesin = pd.read_csv(file)
    print("csv read in:", file)
    linesin = linesin.fillna(' ')
    for index, row in linesin.iterrows():
        insert_record(conn, row)
    print('inserted the records')

conn.close()