import main
import database
import time
from bs4 import BeautifulSoup
import bot_config
import random
from datetime import date,datetime
import traceback,sys
import login
import extract
import pytz

utc = pytz.timezone('UTC')



def get_greeting():
    greetings=bot_config.greetings
    greeting=random.choice(greetings)
    return greeting


def send_message(driver,message,msg_frame_xpath ,chat_frame_xpath ,input_selector,send_selector):
    driver.switch_to.default_content()
    post_frame=driver.find_element_by_xpath(msg_frame_xpath)
    driver.switch_to.frame(post_frame)
    driver.find_element_by_xpath(input_selector).send_keys(message)
    time.sleep(7)
    driver.find_element_by_xpath(send_selector).click()
    driver.switch_to.default_content()
    if chat_frame_xpath is not None:
        chat_frame = driver.find_element_by_xpath(chat_frame_xpath)
        driver.switch_to.frame(chat_frame)
    return driver


def create_results_dict_1(messages):
    results=[]
    now=datetime.now(utc)
    today = str(now.date().today())
    for message in messages:
        soup=BeautifulSoup(str(message),'html.parser')
        chat_text = soup.text
        start_time = chat_text.find('[')
        end_time = chat_text.find(']')
        chat_time = chat_text[start_time + 1:end_time]
        try:
            chat_userid = message['class'][0]
        except KeyError:
            chat_userid = ''
        try:
            chat_bolds = message.find_all('b')
            chat_username = chat_bolds[0].text
        except KeyError:
            chat_username = ''
        pics = message.find_all('a')
        chat_pics = []
        for pic in pics:
            try:
                chat_pics.append(pic['href'])
            except KeyError:
                chat_pics.append("unknown link")
        record = {'userid': chat_userid, 'user_name': chat_username, 'date': today + "-" + chat_time,
                  'image_links': chat_pics,
                  'text': chat_text}
        record = extract.add_flags(record)
        results.append(record)
    return results

def create_results_dict_4(messages):
    results=[]
    now=datetime.now(utc)
    today=str(now.date())
    for message in messages:
        soup=BeautifulSoup(str(message),'html.parser')
        all_text=soup.text
        index1 = all_text.find('-')
        index2 = all_text.find('-', index1 + 1)
        chat_time = all_text[:index1 - 1].strip()
        try:
            x = message.findAll(class_='usermsg')
            if len(x) > 0:
                chat_username = all_text[index1 + 1:index2].strip()
            else:
                chat_username = 'system_message'
        except:
            continue
        chat_text = all_text
        links = message.find_all('a')
        chat_pics = []
        for link in links:
            try:
                chat_pics.append(link['href'])
            except:
                continue
        record = {'userid': 'not available', 'user_name': chat_username, 'date': today + "-" + chat_time,
                  'image_links': chat_pics, 'text': chat_text}
        record=extract.add_flags(record)
        results.append(record)
    return results



def create_results_dict_3(messages):
    results=[]
    now=datetime.now(utc)
    today = now.date()
    year = str(today.year)
    for message in messages:
        print('working on this msg:',message)
        try:
            soup=BeautifulSoup(str(message),'html.parser')
            all_text=soup.text
            month=all_text[:all_text.find('-')]
            day=all_text[all_text.find('-')+1:all_text.find(' ')]
            index1=all_text.find('-',4)
            index2=all_text.find('-',index1+1)
            chat_time=all_text[all_text.find(' '):index1].strip()[:-3]
            try:
                x=message.findAll(class_='usermsg')
                if len(x)>0:
                    chat_username = all_text[index1 + 1:index2].strip()
                else:
                    chat_username = 'system_message'
            except:
                continue
            chat_text=all_text
            links=message.find_all('a')
            chat_pics=[]
            for link in links:
                try:
                    if link.has_attr('class') and 'nicklink' in link['class']:
                        continue
                    elif link.has_attr('title') and link['title']=='Click to public message':
                        continue
                    else:
                        chat_pics.append(link['href'])
                except:
                    continue
            record = {'userid': 'not available', 'user_name': chat_username, 'date': year+'-'+month+'-'+day+'-'+chat_time,
                      'image_links': chat_pics, 'text': chat_text}
            record=extract.add_flags(record)
            results.append(record)
        except Exception as e:
            print('failed to extract from message:', e)
    print('returning these results:',results)
    return results

def create_results_dict_2(messages):
    print('in create dict with these msgs:',messages)
    results=[]
    now=datetime.now(utc)
    today = str(now.date())
    for message in messages:
        print('working on this msg: ',message)
        try:
            soup=BeautifulSoup(str(message),'html.parser')
            print('got this soup: ',soup)
            all_text=soup.text
            index1 = all_text.find('-')
            index2 = all_text.find('-', index1 + 1)
            chat_time = all_text[:index1].strip()[:-3]
            chat_username = all_text[index1 + 1:index2].strip()
            chat_text = all_text
            # links = soup.find_all('a')
            links=soup.select('.usermsg span a')
            print('got these links')
            chat_pics = []
            for link in links:
                try:
                    chat_pics.append(link['href'])
                except:
                    continue
            record = {'userid': 'not available', 'user_name': chat_username, 'date': today + "-" + chat_time,
                      'image_links': chat_pics, 'text': chat_text}
            print('this is the record before adding flags',record)
            record = extract.add_flags(record)
            print('after adding flags', record)
            results.append(record)
        except Exception as e:
            print('failed to extract from message:', e)
    return results


def create_results_dict_5(messages):
    results=[]
    now=datetime.now(utc)
    today = str(now.date())
    for message in messages:
        print('working on this msg:',message)
        try:

            chat_time=message.find_all('t')[0].text[1:-4]
            user_id=str(message['class'])
            all_text=message.text

            index1=all_text.find(']')
            index2=all_text.find(':',index1)
            chat_username=all_text[index1+1:index2].strip()
            if chat_username=='Chat':
                chat_username='system_message'

            chat_text=all_text
            links=message.find_all('a')
            chat_pics=[]
            for link in links:
                try:
                    chat_pics.append(link['href'])
                except:
                    continue
            record = {'userid': user_id, 'user_name': chat_username, 'date': today + "-" + chat_time,
                      'image_links': chat_pics, 'text': chat_text}
            record=extract.add_flags(record)
            results.append(record)
        except Exception as e:
            print('failed to extract from message:', e)
    print('returning these results:',results)
    return results


def create_personal_response(result):
    accolade=random.choice(bot_config.pic_response)
    response="@{sender} thanks, {acc1}".format(sender=result['user_name'],acc1=accolade)
    return response


def get_messages_from_soup(soup,site_name):
    #print('getting soup with this site name: ',site_name)
    if site_name in ["BoysLand","BoysPub","LoliPub","The Public Pedo Club"]:
        messages=soup.find_all(class_=bot_config.paths[site_name]["message_path"])
    elif site_name in ["The Annex","TorPedoChat"]:
        messages=soup.select(bot_config.paths[site_name]["message_path"])
    else:
        messages=[]
    #print('got these messages from soup:' ,messages)
    return messages


def create_general_response():
    return random.choice(bot_config.thank_yous)


def create_low_chat_response():
    return random.choice(bot_config.low_chat)

def update_last_image_result(results,last_image_result):
    for result in results:
        if len(result['image_links'])>0:
            last_image_result=result['image_links'][-1]
    return last_image_result


def take_snapshot(driver,site,chat_frame_xpath):
    driver.switch_to.default_content()
    path=site['name']+'_snapshot.html'
    if chat_frame_xpath is not None:
        chat_frame = driver.find_element_by_xpath(chat_frame_xpath)
        driver.switch_to.frame(chat_frame)
    page=driver.page_source
    with open (path,'w') as f:
        f.write(page)
    return driver


def choose_response_type(messages):
    response_type=0
    #message=None
    for msg in messages:
        if len(msg['image_links'])>2 and not msg['user_name']=='system message':
            print('PERSONAL RESPONSE FOR ',msg,type(msg['image_links']),len(msg['image_links']))
            response_type= 1
            return response_type,msg
    if len(messages)>10:
        response_type= 2
    else:
        response_type= 3
    return response_type,None


def start_bot(driver,results,site):
    time.sleep(60)
    msg_frame_xpath=bot_config.paths[site['name']]['post_frame_xpath']
    chat_frame_xpath=bot_config.paths[site['name']]['chat_frame_xpath']
    greeting = get_greeting()
    input_selector = bot_config.paths[site['name']]['input_selector']
    send_selector = bot_config.paths[site['name']]['send_selector']
    driver=send_message(driver,greeting,msg_frame_xpath,chat_frame_xpath,input_selector, send_selector)
    soup = BeautifulSoup(driver.page_source, 'html.parser')
    messages=get_messages_from_soup(soup,site['name'])
    #messages = soup.find_all(class_=bot_config.paths[site['name']]["message_path"])
    start_index=bot_config.paths[site['name']]['start_index']
    last_line = messages[start_index]
    print('last line', last_line)
    start_time=time.time()
    next_response = random.choice(range(12 * 60, 13 * 60))
    last_image_result={}
    num_of_results_since_last_response=0
    results_since_last_comment = []
    while True:
        driver=take_snapshot(driver,site,chat_frame_xpath)
        time.sleep(60)
        driver.switch_to.default_content()
        if chat_frame_xpath is not None:
            driver.switch_to.frame(driver.find_element_by_xpath(chat_frame_xpath))
        soup=BeautifulSoup(driver.page_source,'html.parser')
        messages = get_messages_from_soup(soup, site['name'])
        curr_line=messages[start_index]
        new_messages = []
        i = start_index
        while (curr_line.text != last_line.text):
            new_messages.append(curr_line)
            if start_index==0:
                i += 1
            if start_index==-1:
                i -=1
            curr_line = messages[i]
            print('curr line: ', i, curr_line)
        last_line=messages[start_index]
        if site['type']==1:
            results=create_results_dict_1(new_messages)
        if site['type'] == 2:
            results = create_results_dict_2(new_messages)
        if site['type'] == 3:
            results = create_results_dict_3(new_messages)
        if site['type'] == 4:
            results= create_results_dict_4(new_messages)
        if site['type'] == 5:
            results = create_results_dict_5(new_messages)
        print('these are the results in the chat bot:',results)
        results_since_last_comment+=results
        curr_image_result=update_last_image_result(results,last_image_result)
        main.store_in_database(results, site['name'])
        #num_of_results_since_last_response+=len(results)
        print ('got these new msgs:' ,new_messages)
        # respond to msgs with pics in them
        elapsed_time=time.time()-start_time
        if elapsed_time>next_response:
            response_type,res_msg=choose_response_type(results_since_last_comment)
            print('response type and msg: ',response_type,res_msg)
            if response_type==1:
                response = create_personal_response(res_msg)
            elif response_type==2:
                response=create_general_response()
            elif response_type==3:
                response=''
            send_message(driver,response,msg_frame_xpath,chat_frame_xpath,input_selector,send_selector)
            results_since_last_comment=[]
            start_time=time.time()
            next_response=random.choice(range(12 * 60, 13 * 60))

        # for result in results:
        #     if len(result['image_links'])>0:
        #         print('found image to respond to')
        #         response=create_pic_response(result)
        #         print('created this response',response,'trying to send')
        #         driver=send_response(driver,msg_frame_xpath,chat_frame_xpath,input_selector,send_selector,response)
        #         break



def start_live_scraper(site):
    driver = main.general_login(site)
    print('this is the driver:', driver)
    #results = main.general_extract(driver, site)
    results=[]
    try:
        start_bot(driver,results,site)
    except Exception as e:
        print('failed in start_torpedo_scraper ',e)
        traceback.print_exc(file=sys.stdout)
        start_bot(driver,results,site)