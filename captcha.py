import base64
from bs4 import BeautifulSoup
import PIL
from PIL import Image, ImageEnhance, ImageOps
import cv2
import pytesseract
import numpy

def save_captcha(driver,captcha_img_wrapper):
    #print('got to captcha func')
    soup = BeautifulSoup(driver.page_source, 'html.parser')
    #print('this is captcha img wrapper',captcha_img_wrapper)
    captcha_el = soup.select(captcha_img_wrapper)[0]
    #print('CE:', captcha_el)
    captcha_img = captcha_el.find('img')
    #print('CI:', captcha_img)
    img_data = captcha_img['src']
    #print('ID:', img_data)
    head, data = img_data.split(',', 1)

    # Get the file extension (gif, jpeg, png)
    file_ext = head.split(';')[0].split('/')[1]

    # Decode the image data
    plain_data = base64.b64decode(data)

    # Write the image to a file
    path='image.'+file_ext
    with open(path, 'wb') as f:
        f.write(plain_data)
    return path

def analyze_file(filename):
    # load the example image and convert it to RGB, invert it and adjust brightness
    image = Image.open(filename).convert('RGB')
    image = PIL.ImageOps.invert(image)
    image = ImageEnhance.Brightness(image)

    image = image.enhance(80)
    imageArray = numpy.array(image)
    imageArray = imageArray[:, :, ::-1].copy()

    # load the image as a PIL/Pillow image, apply OCR, and then delete
    # the temporary file
    text = pytesseract.image_to_string(image)
    if text.strip() == '':
        return '', imageArray
    else:
        return text, imageArray




def get_captcha_results(driver,captcha_img_wrapper):
    path = save_captcha(driver,captcha_img_wrapper)
    text,imageArray=analyze_file(path)
    return text
    #print(text)