from bs4 import BeautifulSoup
from datetime import date,timedelta,datetime
import bot_config
from pytz import timezone
import pytz
import re
import database
import graph_db
import pycountry
from langdetect import detect
from googletrans import Translator

translator=Translator()
utc = pytz.timezone('UTC')


def extract_type5(driver,name):
    chat_xpath=bot_config.paths[name]['chat_frame_xpath']
    chat_frame = driver.find_element_by_xpath(chat_xpath)
    driver.switch_to_frame(chat_frame)
    soup=BeautifulSoup(driver.page_source,'html.parser')
    messages=soup.select('.messages')[0]
    day_switch=messages.find_all('hr')
    now=datetime.now(utc)
    today=str(now.date())
    yesterday=str(date.today()-timedelta(days=1))
    if len(day_switch) > 0:
        day=yesterday
    else:
        day=today
    results=[]
    for message in messages:
        try:
            if message.name == 'hr':
                day = today
                continue
            elif message.name == 'span':
                continue
            elif message.name == 'div':
                chat_time=message.find_all('t')[0].text[1:-4]
                user_id=str(message['class'])
                all_text=message.text
                index1=all_text.find(']')
                index2=all_text.find(':',index1)
                chat_username=all_text[index1+1:index2].strip()
                if chat_username=='Chat':
                    chat_username='system_message'
                chat_text=all_text
                links=message.find_all('a')
                chat_pics=[]
                for link in links:
                    try:
                        chat_pics.append(link['href'])
                    except:
                        continue
            record = {'userid': user_id, 'user_name': chat_username, 'date': day + "-" + chat_time,
                      'image_links': chat_pics, 'text': chat_text}
            record=add_flags(record)
            results.append(record)
        except Exception as e:
            print('failed to extract from message:', e)
    return results


def extract_type4(driver,name):
    now = datetime.now(utc)
    today = str(now.date())
    try:
        soup=BeautifulSoup(driver.page_source,'html.parser')
    except:
        return []
    chat_xpath=bot_config.paths[name]['chat_frame_xpath']
    chat_frame = driver.find_element_by_xpath(chat_xpath)
    driver.switch_to.frame(chat_frame)
    soup=BeautifulSoup(driver.page_source,'html.parser')
    messages=soup.findAll(class_='msg')
    results=[]
    for message in messages:
        try:
            all_text=message.text
            index1=all_text.find('-')
            index2=all_text.find('-',index1+1)
            chat_time=all_text[:index1-1].strip()
            try:
                x=message.findAll(class_='usermsg')
                if len(x)>0:
                    chat_username = all_text[index1 + 1:index2].strip()
                else:
                    chat_username = 'system_message'
            except:
                continue
            chat_text=all_text
            links=message.find_all('a')
            chat_pics=[]
            for link in links:
                try:
                    chat_pics.append(link['href'])
                except:
                    continue
            record = {'userid': 'not available', 'user_name': chat_username, 'date': today + "-" + chat_time,
                      'image_links': chat_pics, 'text': chat_text}
            record=add_flags(record)
            results.append(record)
        except Exception as e:
            print('failed to extract from message:', e)
    return results


def extract_type3(driver,name):
    now=datetime.now(utc)
    today = now.date()
    year= str(today.year)
    try:
        soup=BeautifulSoup(driver.page_source,'html.parser')
    except:
        return []
    chat_xpath=bot_config.paths[name]['chat_frame_xpath']
    chat_frame = driver.find_element_by_xpath(chat_xpath)
    driver.switch_to.frame(chat_frame)
    soup=BeautifulSoup(driver.page_source,'html.parser')
    messages=soup.findAll(class_='msg')
    results=[]
    for message in messages:
        try:
            all_text=message.text
            month=all_text[:all_text.find('-')]
            day=all_text[all_text.find('-')+1:all_text.find(' ')]
            index1=all_text.find('-',4)
            index2=all_text.find('-',index1+1)
            chat_time=all_text[all_text.find(' '):index1].strip()[:-3]
            try:
                x=message.findAll(class_='usermsg')
                if len(x)>0:
                    chat_username = all_text[index1 + 1:index2].strip()
                else:
                    chat_username = 'system_message'
            except:
                continue
            chat_text=all_text
            links=message.find_all('a')
            chat_pics=[]
            for link in links:
                try:
                    if link.has_attr('class') and 'nicklink' in link['class']:
                        continue
                    elif link.has_attr('title') and link['title']=='Click to public message':
                        continue
                    else:
                        chat_pics.append(link['href'])
                except:
                    continue
            record = {'userid': 'not available', 'user_name': chat_username, 'date': year+'-'+month+'-'+day+'-'+chat_time,
                      'image_links': chat_pics, 'text': chat_text}
            record=add_flags(record)
            results.append(record)
        except Exception as e:
            print('failed to extract from message:', e)
    return results

def extract_type2(driver,name):
    now=datetime.now(utc)
    today = str(now.date())
    chat_xpath=bot_config.paths[name]['chat_frame_xpath']
    chat_frame=driver.find_element_by_xpath(chat_xpath)
    driver.switch_to.frame(chat_frame)
    soup=BeautifulSoup(driver.page_source,'html.parser')
    messages=soup.findAll(class_='msg')
    results=[]
    for message in messages:
        try:
            all_text=message.text
            index1=all_text.find('-')
            index2=all_text.find('-',index1+1)
            chat_time=all_text[:index1].strip()[:-3]
            chat_username=all_text[index1+1:index2].strip()
            chat_text=all_text
            links=message.select('.usermsg span a')
            chat_pics=[]
            for link in links:
                try:
                    chat_pics.append(link['href'])
                except:
                    continue
            record = {'userid': 'not available', 'user_name': chat_username, 'date': today + "-" + chat_time,
                      'image_links': chat_pics, 'text': chat_text}
            record=add_flags(record)
            results.append(record)
        except Exception as e:
            print('failed to extract from message:', e)
    return results

def extract_basic_chats(driver):
    now=datetime.now(utc)
    day = str(now.date())
    page = driver.page_source
    try:
        soup = BeautifulSoup(page, 'html.parser')
    except:
        return []
    messages=soup.select("#messages")[0]
    day_switch=messages.find_all('hr')
    if len(day_switch)>0:
        today=str(now.date().today())
        yesterday=str(now.date()-timedelta(days=1))
        day=yesterday
    results = []
    for message in messages:
        if message.name=='hr':
            day=today
            continue
        elif message.name=='span':
            continue
        elif message.name=='p':
            chat_text = message.text
            start_time = chat_text.find('[')
            end_time = chat_text.find(']')
            chat_time = chat_text[start_time + 1:end_time]
            try:
                chat_userid = message['class'][0]
            except:
                chat_userid = ''
            try:
                chat_bolds = message.find_all('b')
                chat_username = chat_bolds[0].text
            except KeyError:
                chat_username = ''
            pics = message.find_all('a')
            chat_pics = []
            for pic in pics:
                try:
                    chat_pics.append(pic['href'])
                except KeyError:
                    chat_pics.append("unknown link")
            record = {'userid': chat_userid, 'user_name': chat_username, 'date': day + "-"+chat_time, 'image_links': chat_pics,
                            'text': chat_text}
            record = add_flags(record)
            results.append(record)
    return results


def get_contacts_from_text(text,all_known_users):
    #all_known_users=database.get_all_users()
    text_split=text.split(' ')
    all_known_contacts=''
    all_unknown_contacts=''
    for index,word in enumerate(text_split):
        if word=="@":
            try:
                #print(word)
                #print(text_split,index)
                word=text_split[index]+text_split[index+1]
                #print(word)
            except:
                continue
        else:
            word=word
        if len(word)>0 and word[0]=="@":
            regex = re.compile('[^a-zA-Z0-9]')
            contact=regex.sub('', word)
            if contact in all_known_users.keys():
                all_known_contacts+=contact+','
            else:
                all_unknown_contacts+=contact+','
    return all_known_contacts[:-1],all_unknown_contacts[:-1]

def add_flags(record):
    #print('in add record',record)
    #textline = record['text'].lower()
    text = record['text'].lower()
    #text = textline.split(':')[2]
    #known_contacts,unknown_contacts=get_contacts_from_text(text)
    record['known_contacts'] = ''
    record['unknown_contacts']=''
    record['boy'] = 0
    if 'boy' in text or '[b]' in text or 'him' in text:
        record['boy'] = 1
    record['girl'] = 0
    if 'girl' in text or '[g]' in text or 'her' in text:
        record['girl'] = 1
    record['hebe'] = 0
    if 'hebe' in text or 'preteen' in text or 'tween' in text:
        record['hebe'] = 1
    record['pedo'] = 0
    if 'pedo' in text or 'teen' in text:
        record['pedo'] = 1
    record['cam'] = 0
    if 'cam' in text:
        record['cam'] = 1
    record['pics'] = 0
    if 'pic' in text:
        record['pics'] = 1
    record['video'] = 0
    if 'video' in text or 'vid' in text:
        record['video'] = 1
    record['rape'] = 0
    if 'rape' in text:
        record['rape'] = 1
    record['baby'] = 0
    if 'baby' in text:
        record['baby'] = 1
    record['dad'] = 0
    if 'dad' in text:
        record['dad'] = 1
    record['parent'] = 0
    if 'parent' in text or 'daughter' in text or 'son' in text:
        record['parent'] = 1
    record['mom'] = 0
    if 'mom' in text:
        record['mom'] = 1
    record['toddler'] = 0
    if 'toddler' in text:
        record['toddler'] = 1
    record['extreme'] = 0
    if 'extreme' in text or 'torture' in text or 'bondage' in text or 'snuf' in text or 'scat' in text or 'gore' in text or 'bdsm' in text or 'hardcore' in text or '[hc]' in text:
        record['extreme'] = 1
    record['buy'] = 0
    if 'buy' in text:
        record['buy'] = 1
    record['sell'] = 0
    if 'sell' in text:
        record['sell'] = 1
    record['album']  = 0
    if 'album' in text or 'gallery' in text or 'spread' in text:
        record['album'] = 1
    record['genitals'] = 0
    if 'cock' in text or 'dick' in text or 'anal' in text or 'bum' in text or 'pussy' in text or 'ass' in text or 'clit' in text or 'cunt' in text or 'peepee' in text:
        record['genitals'] = 1
    record['animal'] = 0
    if 'animal' in text or 'dog' in text or 'zoo' in text:
        record['animal'] = 1
    record['drugs'] = 0
    if 'alcohol' in text or 'weed' in text or 'drug' in text or 'fenatyl' in text:
        record['drugs'] = 1
    record['felixserver'] = 0
    if 'felix' in text:
        record['felixserver'] = 1
    record['pedomomserver'] = 0
    if 'pedomom' in text:
        record['pedomomserver'] = 1
    record['matrixserver'] = 0
    if 'matrix' in text:
        record['matrixserver'] = 1
    record['oral']=0
    if 'blowjob' in text or '[bj]' in text:
        record['oral'] = 1
    record['gang']=0
    if 'gangbang' in text or 'orgy' in text:
        record['gang'] = 1
    record['countries']=get_text_countries(text)
    record['religions']=get_text_religions(text)
    record['language']=get_text_language(text)
    return record


def get_text_language(text):
    language=''
    try:
        detection = translator.detect(text)
    except:
        return language
    lang = detection.lang
    conf = detection.confidence
    if conf > .90:
        language=lang
    return language

def get_text_countries(text):
    found_countries=[]
    words=text.split(' ')
    for word in words:
        if word in bot_config.country_dict.keys():
            found_countries.append(word)
    found_countries=list(set(found_countries))
    found_countries=','.join(found_countries)
    return found_countries


def get_text_religions(text):
    known_religions=bot_config.known_religions
    found_religions=[]
    for religion in known_religions:
        if religion in text:
            found_religions.append(religion)
    found_religions=list(set(found_religions))
    found_religions=','.join(found_religions)
    return found_religions




def is_image(href):
    img_extensions=['.jpg','.png','webp','.jpeg','.gif','bmp']
    if any(ext in href for ext in img_extensions):
        return True
    else:
        return False