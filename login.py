import time
import captcha
import traceback,sys
import bot_config


def manual_login(driver, url):
    try:
        driver.set_page_load_timeout(15)
        driver.get(url)
        print('got url', url)
        driver.implicitly_wait(30)
    except Exception as e:
        return "error"
    return driver


def login_type1(driver, url, name,row):
    site=row['name']
    captcha_wrapper = bot_config.paths[site]["captcha_img_wrapper"]
    try:
        #driver.set_page_load_timeout(15)
        time.sleep(10)
        driver.get(url)
        #driver.implicitly_wait(30)
        driver.find_element_by_name("n").send_keys(name)
        # captcha_results = captcha.get_captcha_results(driver, captcha_wrapper)
        # print('captcha results: ',captcha_results)
        # driver.find_element_by_name(bot_config.paths[site]['captcha_field']).send_keys(captcha_results)
        input("solve captcha,click submit. then click here to continue")
    except Exception as e:
        traceback.print_exc(file=sys.stdout)
        print('got this exception: ',e)
        return "error"
    return driver


def login_type2(driver, url, name, password,row,attempts=0):
    #driver.set_page_load_timeout(30)
    site=row['name']
    captcha_wrapper=bot_config.paths[site]["captcha_img_wrapper"]
    chat_frame_xpath=bot_config.paths[site]['chat_frame_xpath']
    try:
        driver.get(url)
        #driver.implicitly_wait(30)
        driver.find_element_by_name(row['user_name_selector']).send_keys(name)
        #driver.find_element_by_name(row['user_name_selector']).send_keys("PedoDaddy")
        driver.find_element_by_name(row['password_selector']).send_keys(password)
        #time.sleep(20)
        if row['captcha_type'] in [1]:
            if attempts<10:
                captcha_results=captcha.get_captcha_results(driver,captcha_wrapper)
                #print('got these captcha results:',captcha_results)
                # driver.find_element_by_name('captcha').send_keys(captcha_results)
                driver.find_element_by_name(bot_config.paths[site]['captha_field']).send_keys(captcha_results)
                driver.find_element_by_css_selector('input[type="submit"]').click()
                page=driver.page_source
                # print(page)
                h2=driver.find_elements_by_tag_name('h2')
                #print('got these h2:',h2)
                # for i in range(len(h2)):
                #     print(i,h2[i].get_attribute('innerHTML'))
                if len(h2)>0:
                    if h2[0].get_attribute('innerHTML')=='Error: Wrong Captcha':
                        print('got wrong captcha, trying again')
                        attempts+=1
                        login_type2(driver,url,name,password,row,attempts)
                    elif any(msg=='waiting room' for msg in (h.get_attribute('innerHTML').lower() for h in h2)):
                        print('in waiting room')
                        wait=True
                        while wait:
                            print('waiting...')
                            time.sleep(10)
                            try:
                                driver.switch_to.frame(driver.find_element_by_xpath(chat_frame_xpath))
                                messages=driver.find_elements_by_class_name('msg')
                                #print('got these with class msg: ', messages)
                                if len(messages) > 5:
                                    driver.switch_to.default_content()
                                    print('returning driver')
                                    return driver
                            except Exception as e:
                                print('except:',e)
                                continue


                else:
                    chat_frame = driver.find_elements_by_xpath("//frame[@name='view']")
                    #print('got this chat frame: ',chat_frame)
                    if len(chat_frame) > 0:
                        print('returning driver')
                        return driver

            else:
                input('failed captcha 10 times. please solve captcha and click here when chat room has loaded')
                return driver
                #driver.find_element_by_css_selector('input[type="submit"]').click()
        else:
            input('please solve captcha and click here when chat room has loaded')
    except Exception as e:
        traceback.print_exc(file=sys.stdout)
        print ('got this error in get_advanced chat data:',e)
        return 'error'
    return driver


def login_type2_backup(driver, url, name, password,row):
    driver.set_page_load_timeout(30)
    try:
        driver.get(url)
        driver.implicitly_wait(30)
        driver.find_element_by_name(row['user_name_selector']).send_keys(name)
        driver.find_element_by_name(row['password_selector']).send_keys(password)
        #time.sleep(20)
        if row['captcha_type']==1:
            captcha_results=captcha.get_captcha_results(driver)
            driver.find_element_by_name('captcha').send_keys(captcha_results)
            #time.sleep(10)
    except Exception as e:
        print ('got this error in get_advanced chat data:',e)
        return 'error'
    try:
        input('confirm captcha is correct, then click here to continue')
        driver.find_element_by_css_selector('input[type="submit"]').click()
        #time.sleep(5)
        input('click when chat has loaded')
    except Exception as e:
        print(time.asctime())
        print("Except:", driver.session_id,e)
    return driver

def login_type5(driver, url,username):
    try:
        driver.set_page_load_timeout(15)
        driver.get(url)
        print('got url', url)
        driver.implicitly_wait(30)
        input('This page requires manual login. Please click "I don\'t have an account", enter username ({})  then solve captcha and click "Anonymously log in!". Then press any key here to continue'.format(username))
        time.sleep(5)
        a=driver.find_element_by_xpath('//input[@value="Enter"]')
        a.click()
        time.sleep(25)
    except Exception as e:
        print('error in annex login2',e)
        return "error"
    return driver

def login_type4(driver,row):
    driver.get(row['url'])
    driver.find_element_by_name(row['user_name_selector']).send_keys(row['user_name'])
    driver.find_element_by_name(row['password_selector']).send_keys(row['password'])
    driver.find_element_by_xpath('//label[@for="guest"]').click()
    input('solve captcha, login, and click here when chat has loaded')
    # driver.find_element_by_css_selector('input[type="submit"]').click()
    # time.sleep(15)
    # chat_frame = driver.find_elements_by_xpath("//frame[@name='view']")
    # if len(chat_frame)>0:
    #     return driver
    # else:
    #     reload=driver.find_element_by_css_selector('input[value="Reload Messages"]')
    #     print('this is the reload btn:',reload)
    #     reload.click()
    #     #driver.find_element_by_css_selector('input[value="Reload Messages"]').click()
    #     time.sleep(10)
    #     #input('click when chat has loaded')
    return driver