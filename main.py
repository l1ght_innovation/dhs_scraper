from selenium import webdriver
import time
import database
import login
import extract
import graph_db
import sys
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities


def open_selenium(url, browser_type="Chrome", proxy="socks5://localhost:9050"):
    options = webdriver.ChromeOptions()
    options.add_argument('--proxy-server=%s' % proxy)
    try:
        print('tryin to open ',url)
        if browser_type == "Chrome":
            driver = webdriver.Chrome(options=options,executable_path="./chromedriver")
        else:
            driver = webdriver.Firefox(options=options, executable_path='./geckodriver')
        driver.implicitly_wait(30)
        driver.get(url)
    except Exception as e:
        print ('got this error when trying to open url', e)
        return 'error'
    if url =='https://check.torproject.org/':
        page = driver.page_source
        if 'This browser is configured to use Tor.' in page:
            return driver
        else:
            return 'error'



def close_selenium(driver):
    if type(driver) == 'str':
        return
    else:
        driver.quit()
    return


def store_in_database(results, chat_name):
    i=1
    total=len(results)
    print('\n')
    for result in results:
        sys.stdout.write ('\r inserting results into databases %d / %d' % (i,total))
        i+=1
        database.insert_into_chats2(chat_name,result)
        try:
            graph_db.insert_one_line(chat_name,result)
        except Exception as e:
            print ('could not insert line in graph db: ',e)
            with open('graph_err.log', 'a') as f:
                f.write(str(result) + '\n' + str(e) + '\n' + '-----' + '\n')
    database.update_results_contacts(results)
    graph_db.update_contacts_from_scrape(chat_name,results)
    return





def get_driver(browser_type="Chrome", proxy="socks5://localhost:9050",page_load='normal'):
    options = webdriver.ChromeOptions()
    options.add_argument('--proxy-server=%s' % proxy)
    options.page_load_strategy = 'none'
    caps = DesiredCapabilities().CHROME
    caps["pageLoadStrategy"] = page_load
    try:
        if browser_type == "Chrome":
            driver = webdriver.Chrome(desired_capabilities=caps,options=options, executable_path="./chromedriver")
        else:
            driver = webdriver.Firefox(options=options, executable_path='./geckodriver')

        driver.implicitly_wait(30)
        driver.get('https://check.torproject.org/')
        time.sleep(5)
    except Exception as e:
        print('got this error when trying to open url', e)
        return 'error'
    page = driver.page_source
    if 'This browser is configured to use Tor.' in page:
        return driver
    else:
        return 'error'


def general_login(row):
    print("Opening: ",row['name'])
    url = row['url']
    username = row['user_name']
    password = row['password']
    type = row['type']
    if type in [1,5]:
        driver=get_driver(page_load='none')
    else:
        driver=get_driver()
    if driver=='error':
        print('could not connect to tor')
    if type == 1:
        driver=login.login_type1(driver,url,username,row)
    if type in [2,3]:
        driver=login.login_type2(driver, url, username, password,row)
    if type in [4]:
        driver = login.login_type4(driver,row)
        #input('solve capthcha and click login')
    if type in [5]:
        driver = login.login_type5(driver,url,username)
    print("Completed Chat login at", time.asctime())
    return driver

def general_extract(driver,row):
    print("Extracting: ",row['name'])
    type = row['type']
    if type == 1:
        results = extract.extract_basic_chats(driver)
        #store_in_database(results, row['name'])
    if type == 2:
        results=extract.extract_type2(driver,row['name'])
        #store_in_database(results, row['name'])
    if type == 3:
        results=extract.extract_type3(driver,row['name'])
        #store_in_database(results, row['name'])
    if type == 4:
        results=extract.extract_type4(driver,row['name'])
        #store_in_database(results, row['name'])
    if type ==5:
        #print('once chats appear, click x in browser to stop page loading')
        time.sleep(5)
        try:
            results=extract.extract_type5(driver,row['name'])
        except Exception as e:
            print('failed to extract: ',e)
        print('got these results:')
        #store_in_database(results,row['name'])
    store_in_database(results,row['name'])
    print("Completed Chat login at", time.asctime())
    return results




