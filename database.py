import pandas as pd
import pymysql
import pymysql.cursors
import datetime
import extract
import sys

rds_host = "localhost"
rds_name = 'root'
rds_password = ''
rds_db_name = 'DHS'

scrape_id=datetime.datetime.now()

def connect_db():
    conn = pymysql.connect(rds_host, port=3306, user=rds_name, passwd=rds_password, db=rds_db_name,cursorclass=pymysql.cursors.DictCursor)
    return conn

def check_column(conn,col_name):
    cursor=conn.cursor()
    sql="SELECT COUNT(*) as count FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'chats' AND COLUMN_NAME = '{}'".format(col_name)
    cursor.execute(sql)
    res=cursor.fetchall()
    if res[0]['count']==0:
        print('adding col ',col_name)
        sql='ALTER TABLE chats add column {} BOOLEAN'.format(col_name)
        cursor.execute(sql)
        conn.commit()
    print(col_name,res)

def build_sites_table():
    conn=connect_db()
    cursor=conn.cursor()
    df=pd.read_csv('urllists.csv')
    for index,row in df.iterrows():
        name=row['name']
        url=row['url']
        user_name=row['user_name']
        password=row['password']
        timewait=row['timewait']
        send_chat=row['send_chat']
        if send_chat=='Yes':
            send_chat=1
        else:
            send_chat=0
        type=row['type']
        sql="insert into sites (name,url,user_name,password,timewait,send_chat,type) values (%s,%s,%s,%s,%s,%s,%s)"
        val=(name,url,user_name,password,timewait,send_chat,type)
        cursor.execute(sql,val)
        conn.commit()
    conn.close()

def insert_into_chats(site,userid,user_name,date,image_links,text):
    conn=connect_db()
    cursor=conn.cursor()
    date_time=datetime.datetime.strptime(date,'%Y-%m-%d-%H:%M')
    sql="insert into chats (site,userid,user_name,date,image_links,text) values (%s,%s,%s,%s,%s,%s)"
    val=(site,userid,user_name,date_time,str(image_links)[:500],text[:500])
    try:
        cursor.execute(sql,val)
        conn.commit()
    except Exception as e:
        print ('could not insert line',vars())
    conn.close()
    return


def build_sql_query(conn,site,results):
    date_time=datetime.datetime.strptime(results['date'],'%Y-%m-%d-%H:%M')
    results['image_links']=str(results['image_links'])[:500]
    results['text']=results['text'][:500]
    results['userid']=results['userid'][:50]
    results['religions']=results['religions'][:50]
    results['countries']=results['countries'][:50]
    results['scrape_id']=scrape_id
    sql="insert into chats (site,date"
    values=" values (%s,%s"
    vals=(site,date_time)
    for key,val in results.items():
        if key=='date' :
            continue
        sql=sql+","+key
        values=values+',%s'
        vals+=(val,)
    sql+=")"
    values+=")"
    sql_query=sql+values
    return sql_query,vals

def insert_into_chats2(site,results):
    conn = connect_db()
    cursor = conn.cursor()
    sql,val=build_sql_query(conn,site,results)
    try:
        cursor.execute(sql, val)
        conn.commit()
        results['index']=cursor.lastrowid
    except Exception as e:
        print('could not insert line ', e)
    conn.close()
    return results




def get_sites():
    conn=connect_db()
    cursor=conn.cursor()
    sql='select * from sites'
    cursor.execute(sql)
    res=cursor.fetchall()
    conn.close()
    return res



def test_insert():
    a=datetime.datetime.now()
    results=[{'userid': 'not available', 'user_name': 'sonny', 'date': '2020-07-29-11:20', 'image_links': [], 'text': '11:20 - sonnyHdhdhdh😜😀😊😃 - cocksuckers [PedoBum (gallery):Fy76ge]', 'contacts': '', 'boy': 0, 'girl': 0, 'hebe': 0, 'pedo': 0, 'cam': 0, 'pics': 0, 'video': 0, 'rape': 0, 'baby': 0, 'dad': 0, 'parent': 0, 'mom': 0, 'toddler': 0, 'extreme': 0, 'buy': 0, 'sell': 0, 'album': 0, 'genitals': 0, 'animal': 0, 'drugs': 0, 'felixserver': 0, 'pedomomserver': 0, 'matrixserver': 0}, {'userid': 'not available', 'user_name': 'sonny', 'date': '2020-07-29-11:14', 'image_links': [], 'text': '11:14 - sonny - little cock suckers [PedoBum (gallery):8RUwAO]', 'contacts': '', 'boy': 0, 'girl': 0, 'hebe': 0, 'pedo': 0, 'cam': 0, 'pics': 0, 'video': 0, 'rape': 0, 'baby': 0, 'dad': 0, 'parent': 0, 'mom': 0, 'toddler': 0, 'extreme': 0, 'buy': 0, 'sell': 0, 'album': 0, 'genitals': 0, 'animal': 0, 'drugs': 0, 'felixserver': 0, 'pedomomserver': 0, 'matrixserver': 0}, {'userid': 'not available', 'user_name': 'sonny', 'date': '2020-07-29-11:01', 'image_links': [], 'text': '11:01 - sonny - [g] yummy infants [PedoBum (gallery):pR4c45]', 'contacts': '', 'boy': 0, 'girl': 0, 'hebe': 0, 'pedo': 0, 'cam': 0, 'pics': 0, 'video': 0, 'rape': 0, 'baby': 0, 'dad': 0, 'parent': 0, 'mom': 0, 'toddler': 0, 'extreme': 0, 'buy': 0, 'sell': 0, 'album': 0, 'genitals': 0, 'animal': 0, 'drugs': 0, 'felixserver': 0, 'pedomomserver': 0, 'matrixserver': 0}, {'userid': 'not available', 'user_name': 'RoughPedo', 'date': '2020-07-29-10:57', 'image_links': [], 'text': '10:57 - RoughPedo - Hey Nepis! :D Glad to be here and see so many nepis here', 'contacts': '', 'boy': 0, 'girl': 1, 'hebe': 0, 'pedo': 0, 'cam': 0, 'pics': 0, 'video': 0, 'rape': 0, 'baby': 0, 'dad': 0, 'parent': 0, 'mom': 0, 'toddler': 0, 'extreme': 0, 'buy': 0, 'sell': 0, 'album': 0, 'genitals': 0, 'animal': 0, 'drugs': 0, 'felixserver': 0, 'pedomomserver': 0, 'matrixserver': 0}, {'userid': 'not available', 'user_name': 'sonny', 'date': '2020-07-29-10:54', 'image_links': [], 'text': '10:54 - sonny - [g] [PedoBum (gallery):oL8ADu]', 'contacts': '', 'boy': 0, 'girl': 0, 'hebe': 0, 'pedo': 0, 'cam': 0, 'pics': 0, 'video': 0, 'rape': 0, 'baby': 0, 'dad': 0, 'parent': 0, 'mom': 0, 'toddler': 0, 'extreme': 0, 'buy': 0, 'sell': 0, 'album': 0, 'genitals': 0, 'animal': 0, 'drugs': 0, 'felixserver': 0, 'pedomomserver': 0, 'matrixserver': 0}, {'userid': 'not available', 'user_name': 'sonny', 'date': '2020-07-29-10:51', 'image_links': [], 'text': '10:51 - sonny - [g] hope [PedoBum (gallery):wzLXD4]', 'contacts': '', 'boy': 0, 'girl': 0, 'hebe': 0, 'pedo': 0, 'cam': 0, 'pics': 0, 'video': 0, 'rape': 0, 'baby': 0, 'dad': 0, 'parent': 0, 'mom': 0, 'toddler': 0, 'extreme': 0, 'buy': 0, 'sell': 0, 'album': 0, 'genitals': 0, 'animal': 0, 'drugs': 0, 'felixserver': 0, 'pedomomserver': 0, 'matrixserver': 0}, {'userid': 'not available', 'user_name': 'sonny', 'date': '2020-07-29-10:41', 'image_links': [], 'text': '10:41 - sonny - [g] [PedoBum (gallery):oxsmrY]', 'contacts': '', 'boy': 0, 'girl': 0, 'hebe': 0, 'pedo': 0, 'cam': 0, 'pics': 0, 'video': 0, 'rape': 0, 'baby': 0, 'dad': 0, 'parent': 0, 'mom': 0, 'toddler': 0, 'extreme': 0, 'buy': 0, 'sell': 0, 'album': 0, 'genitals': 0, 'animal': 0, 'drugs': 0, 'felixserver': 0, 'pedomomserver': 0, 'matrixserver': 0}, {'userid': 'not available', 'user_name': 'sonny', 'date': '2020-07-29-10:36', 'image_links': [], 'text': '10:36 - sonny - [g] [PedoBum (gallery):TQwAHU]', 'contacts': '', 'boy': 0, 'girl': 0, 'hebe': 0, 'pedo': 0, 'cam': 0, 'pics': 0, 'video': 0, 'rape': 0, 'baby': 0, 'dad': 0, 'parent': 0, 'mom': 0, 'toddler': 0, 'extreme': 0, 'buy': 0, 'sell': 0, 'album': 0, 'genitals': 0, 'animal': 0, 'drugs': 0, 'felixserver': 0, 'pedomomserver': 0, 'matrixserver': 0}, {'userid': 'not available', 'user_name': 'Nuck', 'date': '2020-07-29-09:51', 'image_links': [], 'text': '09:51 - Nuck - hii :D', 'contacts': '', 'boy': 0, 'girl': 0, 'hebe': 0, 'pedo': 0, 'cam': 0, 'pics': 0, 'video': 0, 'rape': 0, 'baby': 0, 'dad': 0, 'parent': 0, 'mom': 0, 'toddler': 0, 'extreme': 0, 'buy': 0, 'sell': 0, 'album': 0, 'genitals': 0, 'animal': 0, 'drugs': 0, 'felixserver': 0, 'pedomomserver': 0, 'matrixserver': 0}, {'userid': 'not available', 'user_name': 'Draco', 'date': '2020-07-29-07:36', 'image_links': ['http://oju4yn237c6hjh42qothvpreqecnqjhtvh4sgn3fqmsdvhu5d5tyspid.onion/i/17ft087qu.jpg?ToddlersAmuse'], 'text': '07:36 - Draco - [GIRL] [Easy:17ft087qu.jpg]', 'contacts': '', 'boy': 0, 'girl': 0, 'hebe': 0, 'pedo': 0, 'cam': 0, 'pics': 0, 'video': 0, 'rape': 0, 'baby': 0, 'dad': 0, 'parent': 0, 'mom': 0, 'toddler': 0, 'extreme': 0, 'buy': 0, 'sell': 0, 'album': 0, 'genitals': 0, 'animal': 0, 'drugs': 0, 'felixserver': 0, 'pedomomserver': 0, 'matrixserver': 0}, {'userid': 'not available', 'user_name': 'underfive', 'date': '2020-07-29-07:25', 'image_links': ['http://bjbp4bla7rnve4b2slrtnm52reqg7lfxw5xgxjpquwu5fvweqinrgxyd.onion/WdxnPH/1amujjcchx41.jpg?ToddlersAmuse'], 'text': '07:25 - underfive - [babygirl] [PedoBum:1amujjcchx41.jpg]', 'contacts': '', 'boy': 0, 'girl': 0, 'hebe': 0, 'pedo': 0, 'cam': 0, 'pics': 0, 'video': 0, 'rape': 0, 'baby': 0, 'dad': 0, 'parent': 0, 'mom': 0, 'toddler': 0, 'extreme': 0, 'buy': 0, 'sell': 0, 'album': 0, 'genitals': 0, 'animal': 0, 'drugs': 0, 'felixserver': 0, 'pedomomserver': 0, 'matrixserver': 0}, {'userid': 'not available', 'user_name': 'underfive', 'date': '2020-07-29-07:24', 'image_links': ['http://bjbp4bla7rnve4b2slrtnm52reqg7lfxw5xgxjpquwu5fvweqinrgxyd.onion/WdxnPH/124474024.jpg?ToddlersAmuse'], 'text': '07:24 - underfive - [babygirl] [PedoBum:124474024.jpg]', 'contacts': '', 'boy': 0, 'girl': 0, 'hebe': 0, 'pedo': 0, 'cam': 0, 'pics': 0, 'video': 0, 'rape': 0, 'baby': 0, 'dad': 0, 'parent': 0, 'mom': 0, 'toddler': 0, 'extreme': 0, 'buy': 0, 'sell': 0, 'album': 0, 'genitals': 0, 'animal': 0, 'drugs': 0, 'felixserver': 0, 'pedomomserver': 0, 'matrixserver': 0}, {'userid': 'not available', 'user_name': 'underfive', 'date': '2020-07-29-07:21', 'image_links': ['http://bjbp4bla7rnve4b2slrtnm52reqg7lfxw5xgxjpquwu5fvweqinrgxyd.onion/WdxnPH/1amujjcchx42.jpg?ToddlersAmuse'], 'text': '07:21 - underfive - [babygirl] [PedoBum:1amujjcchx42.jpg]', 'contacts': '', 'boy': 0, 'girl': 0, 'hebe': 0, 'pedo': 0, 'cam': 0, 'pics': 0, 'video': 0, 'rape': 0, 'baby': 0, 'dad': 0, 'parent': 0, 'mom': 0, 'toddler': 0, 'extreme': 0, 'buy': 0, 'sell': 0, 'album': 0, 'genitals': 0, 'animal': 0, 'drugs': 0, 'felixserver': 0, 'pedomomserver': 0, 'matrixserver': 0}, {'userid': 'not available', 'user_name': 'underfive', 'date': '2020-07-29-07:14', 'image_links': ['http://bjbp4bla7rnve4b2slrtnm52reqg7lfxw5xgxjpquwu5fvweqinrgxyd.onion/WdxnPH/124474024.jpg?ToddlersAmuse'], 'text': '07:14 - underfive - [babygirl] [PedoBum:124474024.jpg]', 'contacts': '', 'boy': 0, 'girl': 0, 'hebe': 0, 'pedo': 0, 'cam': 0, 'pics': 0, 'video': 0, 'rape': 0, 'baby': 0, 'dad': 0, 'parent': 0, 'mom': 0, 'toddler': 0, 'extreme': 0, 'buy': 0, 'sell': 0, 'album': 0, 'genitals': 0, 'animal': 0, 'drugs': 0, 'felixserver': 0, 'pedomomserver': 0, 'matrixserver': 0}, {'userid': 'not available', 'user_name': 'underfive', 'date': '2020-07-29-07:13', 'image_links': ['http://bjbp4bla7rnve4b2slrtnm52reqg7lfxw5xgxjpquwu5fvweqinrgxyd.onion/WdxnPH/1amujjcchx50.jpg?ToddlersAmuse'], 'text': '07:13 - underfive - [babygirl] [PedoBum:1amujjcchx50.jpg]', 'contacts': '', 'boy': 0, 'girl': 0, 'hebe': 0, 'pedo': 0, 'cam': 0, 'pics': 0, 'video': 0, 'rape': 0, 'baby': 0, 'dad': 0, 'parent': 0, 'mom': 0, 'toddler': 0, 'extreme': 0, 'buy': 0, 'sell': 0, 'album': 0, 'genitals': 0, 'animal': 0, 'drugs': 0, 'felixserver': 0, 'pedomomserver': 0, 'matrixserver': 0}, {'userid': 'not available', 'user_name': 'underfive', 'date': '2020-07-29-07:12', 'image_links': ['http://bjbp4bla7rnve4b2slrtnm52reqg7lfxw5xgxjpquwu5fvweqinrgxyd.onion/WdxnPH/1am9kkfsbx9.jpg?ToddlersAmuse'], 'text': '07:12 - underfive - [babygirl] [PedoBum:1am9kkfsbx9.jpg]', 'contacts': '', 'boy': 0, 'girl': 0, 'hebe': 0, 'pedo': 0, 'cam': 0, 'pics': 0, 'video': 0, 'rape': 0, 'baby': 0, 'dad': 0, 'parent': 0, 'mom': 0, 'toddler': 0, 'extreme': 0, 'buy': 0, 'sell': 0, 'album': 0, 'genitals': 0, 'animal': 0, 'drugs': 0, 'felixserver': 0, 'pedomomserver': 0, 'matrixserver': 0}, {'userid': 'not available', 'user_name': 'underfive', 'date': '2020-07-29-07:08', 'image_links': ['http://uoxqi4lrfqztugili7zzgygibs4xstehf5hohtkpyqcoyryweypzkwid.onion/?img=121588011921.jpg&ToddlersAmuse'], 'text': '07:08 - underfive - [girl] nn [Sluggers:121588011921.jpg]', 'contacts': '', 'boy': 0, 'girl': 0, 'hebe': 0, 'pedo': 0, 'cam': 0, 'pics': 0, 'video': 0, 'rape': 0, 'baby': 0, 'dad': 0, 'parent': 0, 'mom': 0, 'toddler': 0, 'extreme': 0, 'buy': 0, 'sell': 0, 'album': 0, 'genitals': 0, 'animal': 0, 'drugs': 0, 'felixserver': 0, 'pedomomserver': 0, 'matrixserver': 0}, {'userid': 'not available', 'user_name': 'underfive', 'date': '2020-07-29-07:01', 'image_links': ['http://pt3ytjzyezlnzxnjujl45n667qjg7v36ktmc5jwxq4vwjf4udsf423yd.onion/SwIwGm/571f49aa72f49d0e93abdebf5f01bf88.jpg?ToddlersAmuse'], 'text': '07:01 - underfive - [girl] [PedoBum:571f49aa72f49d0e.jpg]', 'contacts': '', 'boy': 0, 'girl': 0, 'hebe': 0, 'pedo': 0, 'cam': 0, 'pics': 0, 'video': 0, 'rape': 0, 'baby': 0, 'dad': 0, 'parent': 0, 'mom': 0, 'toddler': 0, 'extreme': 0, 'buy': 0, 'sell': 0, 'album': 0, 'genitals': 0, 'animal': 0, 'drugs': 0, 'felixserver': 0, 'pedomomserver': 0, 'matrixserver': 0}, {'userid': 'not available', 'user_name': 'coco', 'date': '2020-07-29-07:00', 'image_links': ['http://uoxqi4lrfqztugili7zzgygibs4xstehf5hohtkpyqcoyryweypzkwid.onion/?img=21596005680.jpg&ToddlersAmuse'], 'text': '07:00 - coco - [toddg] [Sluggers:21596005680.jpg]', 'contacts': '', 'boy': 0, 'girl': 0, 'hebe': 0, 'pedo': 0, 'cam': 0, 'pics': 0, 'video': 0, 'rape': 0, 'baby': 0, 'dad': 0, 'parent': 0, 'mom': 0, 'toddler': 0, 'extreme': 0, 'buy': 0, 'sell': 0, 'album': 0, 'genitals': 0, 'animal': 0, 'drugs': 0, 'felixserver': 0, 'pedomomserver': 0, 'matrixserver': 0}, {'userid': 'not available', 'user_name': 'badsitter', 'date': '2020-07-29-06:58', 'image_links': [], 'text': '06:58 - badsitter - [Boys] Gerber (94 Images) [PedoBum (gallery):0mS8db]', 'contacts': '', 'boy': 0, 'girl': 0, 'hebe': 0, 'pedo': 0, 'cam': 0, 'pics': 0, 'video': 0, 'rape': 0, 'baby': 0, 'dad': 0, 'parent': 0, 'mom': 0, 'toddler': 0, 'extreme': 0, 'buy': 0, 'sell': 0, 'album': 0, 'genitals': 0, 'animal': 0, 'drugs': 0, 'felixserver': 0, 'pedomomserver': 0, 'matrixserver': 0}, {'userid': 'not available', 'user_name': 'underfive', 'date': '2020-07-29-06:58', 'image_links': ['http://uoxqi4lrfqztugili7zzgygibs4xstehf5hohtkpyqcoyryweypzkwid.onion/?img=241596005534.jpg&ToddlersAmuse'], 'text': '06:58 - underfive - [girl] [Sluggers:241596005534.jpg]', 'contacts': '', 'boy': 0, 'girl': 0, 'hebe': 0, 'pedo': 0, 'cam': 0, 'pics': 0, 'video': 0, 'rape': 0, 'baby': 0, 'dad': 0, 'parent': 0, 'mom': 0, 'toddler': 0, 'extreme': 0, 'buy': 0, 'sell': 0, 'album': 0, 'genitals': 0, 'animal': 0, 'drugs': 0, 'felixserver': 0, 'pedomomserver': 0, 'matrixserver': 0}, {'userid': 'not available', 'user_name': 'olga', 'date': '2020-07-29-06:49', 'image_links': ['http://jyfilvmxy6ccfrwjzydl2fwpyk5alcsf7xlzl2a5mywkfh73c32mtrid.onion/QmoZa1/1419345951596.jpg?ToddlersAmuse'], 'text': '06:49 - olga - [PedoBum:1419345951596.jpg]', 'contacts': '', 'boy': 0, 'girl': 0, 'hebe': 0, 'pedo': 0, 'cam': 0, 'pics': 0, 'video': 0, 'rape': 0, 'baby': 0, 'dad': 0, 'parent': 0, 'mom': 0, 'toddler': 0, 'extreme': 0, 'buy': 0, 'sell': 0, 'album': 0, 'genitals': 0, 'animal': 0, 'drugs': 0, 'felixserver': 0, 'pedomomserver': 0, 'matrixserver': 0}, {'userid': 'not available', 'user_name': 'Draco', 'date': '2020-07-29-06:49', 'image_links': ['http://oju4yn237c6hjh42qothvpreqecnqjhtvh4sgn3fqmsdvhu5d5tyspid.onion/i/17ft087qu.jpg?ToddlersAmuse'], 'text': '06:49 - Draco - [girl] [Easy:17ft087qu.jpg]', 'contacts': '', 'boy': 0, 'girl': 0, 'hebe': 0, 'pedo': 0, 'cam': 0, 'pics': 0, 'video': 0, 'rape': 0, 'baby': 0, 'dad': 0, 'parent': 0, 'mom': 0, 'toddler': 0, 'extreme': 0, 'buy': 0, 'sell': 0, 'album': 0, 'genitals': 0, 'animal': 0, 'drugs': 0, 'felixserver': 0, 'pedomomserver': 0, 'matrixserver': 0}, {'userid': 'not available', 'user_name': 'coco', 'date': '2020-07-29-06:45', 'image_links': ['http://uoxqi4lrfqztugili7zzgygibs4xstehf5hohtkpyqcoyryweypzkwid.onion/?img=261596004946.jpg&ToddlersAmuse'], 'text': '06:45 - coco - [toddg] [Sluggers:261596004946.jpg]', 'contacts': '', 'boy': 0, 'girl': 0, 'hebe': 0, 'pedo': 0, 'cam': 0, 'pics': 0, 'video': 0, 'rape': 0, 'baby': 0, 'dad': 0, 'parent': 0, 'mom': 0, 'toddler': 0, 'extreme': 0, 'buy': 0, 'sell': 0, 'album': 0, 'genitals': 0, 'animal': 0, 'drugs': 0, 'felixserver': 0, 'pedomomserver': 0, 'matrixserver': 0}, {'userid': 'not available', 'user_name': 'toddboytoes', 'date': '2020-07-29-06:43', 'image_links': [], 'text': '06:43 - toddboytoes - any boys? :SD', 'contacts': '', 'boy': 0, 'girl': 0, 'hebe': 0, 'pedo': 0, 'cam': 0, 'pics': 0, 'video': 0, 'rape': 0, 'baby': 0, 'dad': 0, 'parent': 0, 'mom': 0, 'toddler': 0, 'extreme': 0, 'buy': 0, 'sell': 0, 'album': 0, 'genitals': 0, 'animal': 0, 'drugs': 0, 'felixserver': 0, 'pedomomserver': 0, 'matrixserver': 0}, {'userid': 'not available', 'user_name': 'LoveLittleTodds', 'date': '2020-07-29-06:43', 'image_links': ['http://3b6clio4syptsnvvtzyxifqvtizyazgyowpp3v5f7dj3mzmfhyoy4iyd.onion/images/b772b2eb982f46cf7290b8d3bb2b8afa.jpg?ToddlersAmuse'], 'text': '06:43 - LoveLittleTodds - [toddg] [HDDoro:2b8afa.jpg]', 'contacts': '', 'boy': 0, 'girl': 0, 'hebe': 0, 'pedo': 0, 'cam': 0, 'pics': 0, 'video': 0, 'rape': 0, 'baby': 0, 'dad': 0, 'parent': 0, 'mom': 0, 'toddler': 0, 'extreme': 0, 'buy': 0, 'sell': 0, 'album': 0, 'genitals': 0, 'animal': 0, 'drugs': 0, 'felixserver': 0, 'pedomomserver': 0, 'matrixserver': 0}, {'userid': 'not available', 'user_name': 'badsitter', 'date': '2020-07-29-06:41', 'image_links': [], 'text': '06:41 - badsitter - Sam PedoBum Galleries, 6 Different Sets:[Girl] Sam 1 of 6 (30 Images) [PedoBum (gallery):u1z3H2][Girl] Sam 2 of 6 (15 Images) [PedoBum (gallery):PVdRM6][Girl] Sam 3 of 6 (12 Images) [PedoBum (gallery):Hkkvwv][Girl] Sam 4 of 6 (20 Images) [PedoBum (gallery):jgFEve][Girl] Sam 5 of 6 (16 Images) [PedoBum (gallery):2hoNS0][Girl] Sam 6 of 6 (13 Images) [PedoBum (gallery):udglp3]', 'contacts': '', 'boy': 0, 'girl': 1, 'hebe': 0, 'pedo': 1, 'cam': 0, 'pics': 0, 'video': 0, 'rape': 0, 'baby': 0, 'dad': 0, 'parent': 0, 'mom': 0, 'toddler': 0, 'extreme': 0, 'buy': 0, 'sell': 0, 'album': 1, 'genitals': 1, 'animal': 0, 'drugs': 0, 'felixserver': 0, 'pedomomserver': 0, 'matrixserver': 0}, {'userid': 'not available', 'user_name': 'coco', 'date': '2020-07-29-06:39', 'image_links': ['http://uoxqi4lrfqztugili7zzgygibs4xstehf5hohtkpyqcoyryweypzkwid.onion/?img=341596004723.jpg&ToddlersAmuse'], 'text': '06:39 - coco - [toddg] [Sluggers:341596004723.jpg]', 'contacts': '', 'boy': 0, 'girl': 0, 'hebe': 0, 'pedo': 0, 'cam': 0, 'pics': 0, 'video': 0, 'rape': 0, 'baby': 0, 'dad': 0, 'parent': 0, 'mom': 0, 'toddler': 0, 'extreme': 0, 'buy': 0, 'sell': 0, 'album': 0, 'genitals': 0, 'animal': 0, 'drugs': 0, 'felixserver': 0, 'pedomomserver': 0, 'matrixserver': 0}, {'userid': 'not available', 'user_name': 'Draco', 'date': '2020-07-29-06:37', 'image_links': ['http://oju4yn237c6hjh42qothvpreqecnqjhtvh4sgn3fqmsdvhu5d5tyspid.onion/i/17fsv130t.jpg?ToddlersAmuse'], 'text': '06:37 - Draco - [girl] [Easy:17fsv130t.jpg]', 'contacts': '', 'boy': 0, 'girl': 0, 'hebe': 0, 'pedo': 0, 'cam': 0, 'pics': 0, 'video': 0, 'rape': 0, 'baby': 0, 'dad': 0, 'parent': 0, 'mom': 0, 'toddler': 0, 'extreme': 0, 'buy': 0, 'sell': 0, 'album': 0, 'genitals': 0, 'animal': 0, 'drugs': 0, 'felixserver': 0, 'pedomomserver': 0, 'matrixserver': 0}, {'userid': 'not available', 'user_name': 'badsitter', 'date': '2020-07-29-06:33', 'image_links': [], 'text': '06:33 - badsitter - [Girl] Sor (58 Images) [PedoBum (gallery):6GQn2A]', 'contacts': '', 'boy': 0, 'girl': 0, 'hebe': 0, 'pedo': 0, 'cam': 0, 'pics': 0, 'video': 0, 'rape': 0, 'baby': 0, 'dad': 0, 'parent': 0, 'mom': 0, 'toddler': 0, 'extreme': 0, 'buy': 0, 'sell': 0, 'album': 0, 'genitals': 0, 'animal': 0, 'drugs': 0, 'felixserver': 0, 'pedomomserver': 0, 'matrixserver': 0}, {'userid': 'not available', 'user_name': 'coco', 'date': '2020-07-29-06:30', 'image_links': ['http://uoxqi4lrfqztugili7zzgygibs4xstehf5hohtkpyqcoyryweypzkwid.onion/?img=81596004075.jpg&ToddlersAmuse'], 'text': '06:30 - coco - [babyg] close up [Sluggers:81596004075.jpg]', 'contacts': '', 'boy': 0, 'girl': 0, 'hebe': 0, 'pedo': 0, 'cam': 0, 'pics': 0, 'video': 0, 'rape': 0, 'baby': 0, 'dad': 0, 'parent': 0, 'mom': 0, 'toddler': 0, 'extreme': 0, 'buy': 0, 'sell': 0, 'album': 0, 'genitals': 0, 'animal': 0, 'drugs': 0, 'felixserver': 0, 'pedomomserver': 0, 'matrixserver': 0}, {'userid': 'not available', 'user_name': 'olga', 'date': '2020-07-29-06:27', 'image_links': ['http://uumvs2jwiynozpnr3kcyv77agtqni6symkbmqf4njpau2s42lhoqxkyd.onion/2575Pp/bonjourx001.jpg?ToddlersAmuse'], 'text': '06:27 - olga - [PedoBum:bonjourx001.jpg] mmmmm', 'contacts': '', 'boy': 0, 'girl': 0, 'hebe': 0, 'pedo': 0, 'cam': 0, 'pics': 0, 'video': 0, 'rape': 0, 'baby': 0, 'dad': 0, 'parent': 0, 'mom': 0, 'toddler': 0, 'extreme': 0, 'buy': 0, 'sell': 0, 'album': 0, 'genitals': 0, 'animal': 0, 'drugs': 0, 'felixserver': 0, 'pedomomserver': 0, 'matrixserver': 0}, {'userid': 'not available', 'user_name': 'badsitter', 'date': '2020-07-29-06:26', 'image_links': [], 'text': '06:26 - badsitter - [Girl] Bonjour (59 Images) [PedoBum (gallery):2575Pp]', 'contacts': '', 'boy': 0, 'girl': 0, 'hebe': 0, 'pedo': 0, 'cam': 0, 'pics': 0, 'video': 0, 'rape': 0, 'baby': 0, 'dad': 0, 'parent': 0, 'mom': 0, 'toddler': 0, 'extreme': 0, 'buy': 0, 'sell': 0, 'album': 0, 'genitals': 0, 'animal': 0, 'drugs': 0, 'felixserver': 0, 'pedomomserver': 0, 'matrixserver': 0}, {'userid': 'not available', 'user_name': 'travi', 'date': '2020-07-29-06:23', 'image_links': [], 'text': '06:23 - travi - [g] [Pasta:fee-airport]', 'contacts': '', 'boy': 0, 'girl': 0, 'hebe': 0, 'pedo': 0, 'cam': 0, 'pics': 0, 'video': 0, 'rape': 0, 'baby': 0, 'dad': 0, 'parent': 0, 'mom': 0, 'toddler': 0, 'extreme': 0, 'buy': 0, 'sell': 0, 'album': 0, 'genitals': 0, 'animal': 0, 'drugs': 0, 'felixserver': 0, 'pedomomserver': 0, 'matrixserver': 0}, {'userid': 'not available', 'user_name': 'badsitter', 'date': '2020-07-29-06:23', 'image_links': [], 'text': '06:23 - badsitter - [Girl] Dee (26 Images) [PedoBum (gallery):peF9KW]', 'contacts': '', 'boy': 0, 'girl': 0, 'hebe': 0, 'pedo': 0, 'cam': 0, 'pics': 0, 'video': 0, 'rape': 0, 'baby': 0, 'dad': 0, 'parent': 0, 'mom': 0, 'toddler': 0, 'extreme': 0, 'buy': 0, 'sell': 0, 'album': 0, 'genitals': 0, 'animal': 0, 'drugs': 0, 'felixserver': 0, 'pedomomserver': 0, 'matrixserver': 0}, {'userid': 'not available', 'user_name': 'badsitter', 'date': '2020-07-29-06:20', 'image_links': [], 'text': '06:20 - badsitter - [Girl] 5yo BJ (15 Images) [PedoBum (gallery):9pXCRI]', 'contacts': '', 'boy': 0, 'girl': 0, 'hebe': 0, 'pedo': 0, 'cam': 0, 'pics': 0, 'video': 0, 'rape': 0, 'baby': 0, 'dad': 0, 'parent': 0, 'mom': 0, 'toddler': 0, 'extreme': 0, 'buy': 0, 'sell': 0, 'album': 0, 'genitals': 0, 'animal': 0, 'drugs': 0, 'felixserver': 0, 'pedomomserver': 0, 'matrixserver': 0}, {'userid': 'not available', 'user_name': 'pintinho', 'date': '2020-07-29-06:20', 'image_links': ['http://3b6clio4syptsnvvtzyxifqvtizyazgyowpp3v5f7dj3mzmfhyoy4iyd.onion/images/d44c2cb6c77fa8bebd8a8230fcbf148d.jpg?ToddlersAmuse'], 'text': '06:20 - pintinho - [boy] [HDDoro:bf148d.jpg]', 'contacts': '', 'boy': 0, 'girl': 0, 'hebe': 0, 'pedo': 0, 'cam': 0, 'pics': 0, 'video': 0, 'rape': 0, 'baby': 0, 'dad': 0, 'parent': 0, 'mom': 0, 'toddler': 0, 'extreme': 0, 'buy': 0, 'sell': 0, 'album': 0, 'genitals': 0, 'animal': 0, 'drugs': 0, 'felixserver': 0, 'pedomomserver': 0, 'matrixserver': 0}, {'userid': 'not available', 'user_name': 'travi', 'date': '2020-07-29-06:17', 'image_links': ['http://3b6clio4syptsnvvtzyxifqvtizyazgyowpp3v5f7dj3mzmfhyoy4iyd.onion/images/522dbe916669c78e15cba360575f13f6.jpg?ToddlersAmuse'], 'text': '06:17 - travi - infant sucking a dick [HDDoro:5f13f6.jpg]', 'contacts': '', 'boy': 0, 'girl': 0, 'hebe': 0, 'pedo': 0, 'cam': 0, 'pics': 0, 'video': 0, 'rape': 0, 'baby': 0, 'dad': 0, 'parent': 0, 'mom': 0, 'toddler': 0, 'extreme': 0, 'buy': 0, 'sell': 0, 'album': 0, 'genitals': 0, 'animal': 0, 'drugs': 0, 'felixserver': 0, 'pedomomserver': 0, 'matrixserver': 0}, {'userid': 'not available', 'user_name': 'badsitter', 'date': '2020-07-29-06:17', 'image_links': [], 'text': '06:17 - badsitter - [Girl] Akanbe 2002 (58 Images) [PedoBum (gallery):Oqmbyj]', 'contacts': '', 'boy': 0, 'girl': 0, 'hebe': 0, 'pedo': 0, 'cam': 0, 'pics': 0, 'video': 0, 'rape': 0, 'baby': 0, 'dad': 0, 'parent': 0, 'mom': 0, 'toddler': 0, 'extreme': 0, 'buy': 0, 'sell': 0, 'album': 0, 'genitals': 0, 'animal': 0, 'drugs': 0, 'felixserver': 0, 'pedomomserver': 0, 'matrixserver': 0}, {'userid': 'not available', 'user_name': 'travi', 'date': '2020-07-29-06:15', 'image_links': ['http://3b6clio4syptsnvvtzyxifqvtizyazgyowpp3v5f7dj3mzmfhyoy4iyd.onion/images/2a110443d244ff81b53d2fe6d5a0a7e5.jpg?ToddlersAmuse'], 'text': '06:15 - travi - [HDDoro:a0a7e5.jpg]', 'contacts': '', 'boy': 0, 'girl': 0, 'hebe': 0, 'pedo': 0, 'cam': 0, 'pics': 0, 'video': 0, 'rape': 0, 'baby': 0, 'dad': 0, 'parent': 0, 'mom': 0, 'toddler': 0, 'extreme': 0, 'buy': 0, 'sell': 0, 'album': 0, 'genitals': 0, 'animal': 0, 'drugs': 0, 'felixserver': 0, 'pedomomserver': 0, 'matrixserver': 0}, {'userid': 'not available', 'user_name': 'travi', 'date': '2020-07-29-06:12', 'image_links': ['http://3b6clio4syptsnvvtzyxifqvtizyazgyowpp3v5f7dj3mzmfhyoy4iyd.onion/images/6f102a88fd3c7360369ad71c89ea3401.jpg?ToddlersAmuse'], 'text': '06:12 - travi - no teeth and they suck hard [HDDoro:ea3401.jpg]', 'contacts': '', 'boy': 0, 'girl': 0, 'hebe': 0, 'pedo': 0, 'cam': 0, 'pics': 0, 'video': 0, 'rape': 0, 'baby': 0, 'dad': 0, 'parent': 0, 'mom': 0, 'toddler': 0, 'extreme': 0, 'buy': 0, 'sell': 0, 'album': 0, 'genitals': 0, 'animal': 0, 'drugs': 0, 'felixserver': 0, 'pedomomserver': 0, 'matrixserver': 0}, {'userid': 'not available', 'user_name': 'travi', 'date': '2020-07-29-06:05', 'image_links': ['http://3b6clio4syptsnvvtzyxifqvtizyazgyowpp3v5f7dj3mzmfhyoy4iyd.onion/images/bbe4ce7f12dda14e8b2bce093c4a48ab.jpg?ToddlersAmuse'], 'text': '06:05 - travi - froasted baby [HDDoro:4a48ab.jpg]', 'contacts': '', 'boy': 0, 'girl': 0, 'hebe': 0, 'pedo': 0, 'cam': 0, 'pics': 0, 'video': 0, 'rape': 0, 'baby': 0, 'dad': 0, 'parent': 0, 'mom': 0, 'toddler': 0, 'extreme': 0, 'buy': 0, 'sell': 0, 'album': 0, 'genitals': 0, 'animal': 0, 'drugs': 0, 'felixserver': 0, 'pedomomserver': 0, 'matrixserver': 0}, {'userid': 'not available', 'user_name': 'travi', 'date': '2020-07-29-06:04', 'image_links': ['http://3b6clio4syptsnvvtzyxifqvtizyazgyowpp3v5f7dj3mzmfhyoy4iyd.onion/images/0755c30414ae086d758a70a358dfeca1.jpg?ToddlersAmuse'], 'text': '06:04 - travi - [HDDoro:dfeca1.jpg]', 'contacts': '', 'boy': 0, 'girl': 0, 'hebe': 0, 'pedo': 0, 'cam': 0, 'pics': 0, 'video': 0, 'rape': 0, 'baby': 0, 'dad': 0, 'parent': 0, 'mom': 0, 'toddler': 0, 'extreme': 0, 'buy': 0, 'sell': 0, 'album': 0, 'genitals': 0, 'animal': 0, 'drugs': 0, 'felixserver': 0, 'pedomomserver': 0, 'matrixserver': 0}, {'userid': 'not available', 'user_name': 'badsitter', 'date': '2020-07-29-06:03', 'image_links': [], 'text': '06:03 - badsitter - [Girl] Matsons 5yo (16 Images) [PedoBum (gallery):UdPgte]', 'contacts': '', 'boy': 0, 'girl': 0, 'hebe': 0, 'pedo': 0, 'cam': 0, 'pics': 0, 'video': 0, 'rape': 0, 'baby': 0, 'dad': 0, 'parent': 0, 'mom': 0, 'toddler': 0, 'extreme': 0, 'buy': 0, 'sell': 0, 'album': 0, 'genitals': 0, 'animal': 0, 'drugs': 0, 'felixserver': 0, 'pedomomserver': 0, 'matrixserver': 0}, {'userid': 'not available', 'user_name': 'travi', 'date': '2020-07-29-06:00', 'image_links': ['http://3b6clio4syptsnvvtzyxifqvtizyazgyowpp3v5f7dj3mzmfhyoy4iyd.onion/images/fd7f1b841e71adbcc7732500e99691e8.jpg?ToddlersAmuse'], 'text': '06:00 - travi - [HDDoro:9691e8.jpg]', 'contacts': '', 'boy': 0, 'girl': 0, 'hebe': 0, 'pedo': 0, 'cam': 0, 'pics': 0, 'video': 0, 'rape': 0, 'baby': 0, 'dad': 0, 'parent': 0, 'mom': 0, 'toddler': 0, 'extreme': 0, 'buy': 0, 'sell': 0, 'album': 0, 'genitals': 0, 'animal': 0, 'drugs': 0, 'felixserver': 0, 'pedomomserver': 0, 'matrixserver': 0}]
    for res in results:
        res['scrape_id']=a
        site='test_insert'
        insert_into_chats2(site,res)

def get_all_chats():
    conn=connect_db()
    cursor=conn.cursor()
    sql="select * from chats where scrape_id>'2020-08-08 16:35:38'"
    cursor.execute(sql)
    res=cursor.fetchall()
    df=pd.DataFrame(res)
    df.to_csv('recent_chats.csv',index=False)


def get_count_by_scrape_and_site():
    conn=connect_db()
    cursor=conn.cursor()
    sql="select count(*) as count,scrape_id,site from chats as count group by scrape_id,site order by scrape_id"
    cursor.execute(sql)
    res=cursor.fetchall()
    df=pd.DataFrame(res)
    df.to_csv('site_count.csv',index=False)


def get_avg_chats_per_min():
    conn=connect_db()
    cursor=conn.cursor()
    sql="select scrape_id from chats order by scrape_id desc"
    cursor.execute(sql)
    results=cursor.fetchall()
    results=results[:5]
    res_df=pd.DataFrame()
    for res in results:
        scrape_id=res['scrape_id']
        sql="select * from chats where scrape_id='{}'".format(scrape_id)
        cursor.execute(sql)
        results=cursor.fetchall()
        df=pd.DataFrame(results)
        sites=df.site.unique()
        print(sites)
        for site in sites:
            df_site=df[df['site']==site]
            num_of_texts=len(df_site['text'])
            min_time=df_site.date.min()
            max_time=df_site.date.max()
            elapsed_time=max_time-min_time
            elapsed_time_in_minutes=float(elapsed_time.seconds)/60
            avg=num_of_texts/elapsed_time_in_minutes
            new_line={"site":site,"num_of_texts":num_of_texts,"avg_texts_per_minutes":avg,"min_time":min_time,"max_time":max_time,"elapsed_time":elapsed_time_in_minutes,"scrape_id":scrape_id}
            res_df=res_df.append(new_line,ignore_index=True)
            print(new_line)
    res_df.to_csv('chat_averages.csv',index=False)

def get_contacts():
    conn=connect_db()
    cursor=conn.cursor()
    sql="select chats.index,user_name,text from chats"
    cursor.execute(sql)
    results=cursor.fetchall()
    for result in results:
        index=result['index']
        text=result['text']
        text_split=text.split(' ')
        contacts=[]
        for word in text_split:
            if len(word) > 0 and word[0]=='@':
                contacts.append(word[1:])
        print(contacts)
        sql="""update chats set contacts="%s" where chats.index=%s"""
        vals=(contacts,index)
        cursor.execute(sql,vals)
        conn.commit()
    conn.close()


def get_all_users():
    conn=connect_db()
    cursor=conn.cursor()
    sql="select user_name from chats"
    cursor.execute(sql)
    results=cursor.fetchall()
    users=[]
    for res in results:
        users.append(res['user_name'])
    user_dict=dict.fromkeys(users,1)
    conn.close()
    return (user_dict)


def update_results_contacts(results):
    conn=connect_db()
    cursor=conn.cursor()
    all_known_users=get_all_users()
    i=1
    total=len(results)
    print('\n')
    for res in results:
        #print('updating sql contacts ',i,'/',total)
        sys.stdout.write ('\r updating contacts in sql database %d / %d' % (i,total))
        i+=1
        try:
            index=res['index']
        except:
            continue
        text=res['text']
        known_contacts,unknown_contacts=extract.get_contacts_from_text(text,all_known_users)
        sql="update chats set known_contacts='%s', unknown_contacts='%s' where chats.index=%s"
        vals=known_contacts,unknown_contacts,index
        cursor.execute(sql%vals)
        conn.commit()
    conn.close()

def update_all_contacts():
    conn=connect_db()
    cursor=conn.cursor()
    sql="select chats.index,text from chats"
    cursor.execute(sql)
    results=cursor.fetchall()
    all_known_users=get_all_users()
    i=0
    total=len(results)
    for res in results:
        print(i,'/',total)
        i+=1
        index=res['index']
        text=res['text']
        known_contacts,unknown_contacts=extract.get_contacts_from_text(text,all_known_users)
        sql="update chats set known_contacts='%s', unknown_contacts='%s' where chats.index=%s"
        vals=known_contacts,unknown_contacts,index
        print(sql%vals)
        cursor.execute(sql%vals)
        conn.commit()
    cursor.close()

#if __name__=="__main__":
    #get_avg_chats_per_min()
    #get_all_chats()
    #get_contacts()
    #get_all_chats()
    #update_all_contacts()
    #get_all_users()