import main
import database
import extract
import pandas as pd
import chat_bot
import json
import traceback,sys
import threading
import chat_bot
import multiprocessing
from joblib import Parallel, delayed
from pick import pick
from collections import Counter
import graph_db
import datetime
import pycountry
import bot_config
import ast
from langdetect import detect
import pprint
from textblob import TextBlob
from googletrans import Translator
from bs4 import BeautifulSoup


def test():
    print('testing....')
    sites = database.get_sites()
    for site in sites:
        print(site['name'])
        if site['name'] in ["TorPedoChat","BoysPub","The Annex"]:
        #if site['type'] in [1,2,3,4,5]:
        #if site['type'] in [3]:
            try:
                driver=main.general_login(site)
                print('this is the driver:',driver)
                site['driver']=driver
                results=main.general_extract(driver,site)
                try:
                    driver.close()
                except:
                    print('no driver to close')
                print(results)
                df=pd.DataFrame(results)
                df.to_csv('{}_test.csv'.format(site['name']),index=False)
            except Exception as e:
                traceback.print_exc(file=sys.stdout)
                print('failed on ',site['name'],e)
                continue
        else:
            continue



def start_chatbot(site):
    driver=main.general_login(site)
    #results = main.general_extract(driver, site)
    results=[]
    try:
        chat_bot.start_bot(driver,results,site)
    except Exception as e:
        print('failed in start_chatbot',e)
        traceback.print_exc(file=sys.stdout)
        chat_bot.start_bot(driver,results,site)


def run_chatbot():
    sites = database.get_sites()
    sites_to_scrape=[]
    options=[]
    for site in sites:
        if site['type'] in [1,2,3,4,5]:
            options.append(site['name'])
    title="Please select sites to scrape. Scroll with arrow keys, select with spacebar, enter to submit "
    selected = pick(options,title, min_selection_count=1)
    picked_site=[site for site in sites if site['name'] == selected[0]][0]
    start_chatbot(picked_site)

def run_scraper():
    sites = database.get_sites()
    options=['all']
    sites_to_scrape=[]
    picked_sites=[]
    for site in sites:
        if site['type'] in [1,2,3,4,5]:
            options.append(site['name'])
    #print('options:::',options)
    title="Please select sites to scrape. Scroll with arrow keys, select with spacebar, enter to submit "
    selected = pick(options, title, multiselect=True, min_selection_count=1)
    print(selected)
    if selected[0][0]=='all':
        sites_to_scrape=[site for site in sites if site['type'] in [1,2,3,4,5]]
        print ('sites to scrape: ',sites_to_scrape)
    else:
        for selection in selected:
            picked_sites.append(selection[0])
        for site in sites:
            if site['name'] in picked_sites:
                sites_to_scrape.append(site)
    for site in sites_to_scrape:
        try:
            driver = main.general_login(site)
            #print('this is the driver:', driver)
            site['driver'] = driver
            results = main.general_extract(driver, site)
            try:
                driver.close()
            except:
                print('no driver to close')
            print(results)
            #df = pd.DataFrame(results)
            #df.to_csv('{}_test.csv'.format(site['name']), index=False)
        except Exception as e:
            traceback.print_exc(file=sys.stdout)
            print('failed on ', site['name'], e)
            continue

def test_contact_extraction():
    counter=Counter()
    conn=database.connect_db()
    cursor=conn.cursor()
    sql="select text from chats"
    cursor.execute(sql)
    results=cursor.fetchall()
    i=0
    total=len(results)
    for res in results:
        i+=1
        contacts=extract.get_contacts_from_text(res['text'])
        if len(contacts)>0:
            counter[contacts]+=1
        #print(i,'/',total)
    with open ('test_contacts.json','w') as f:
        f.write(json.dumps(counter,indent=2))
    print(len(counter))


def start_program():
    title = "Please select in-and-out extraction, or keep-alive chatbot connection"
    options=['in and out!','keep-alive chatbot']
    selected = pick(options, title, min_selection_count=1)
    if selected[1]==0:
        run_scraper()
    elif selected[1]==1:
        run_chatbot()
    else:
        print('could not make selection')
        return


def init_graph_from_sql():
    conn=database.connect_db()
    cursor=conn.cursor()
    sql="select * from chats where date >= '2020-08-01' "
    cursor.execute(sql)
    results=cursor.fetchall()
    #print(results)
    print(len(results))
    i=0
    for result in results:
        print(i,'/',len(results))
        i+=1
        #print(result['date'])
        result['date']=datetime.datetime.strftime(result['date'],'%Y-%m-%d-%H:%M:%S')[:-3]
        #print(result['date'],type(result['date']))
        #result['date']=str(result['date'])
        site=result['site']
        try:
            graph_db.insert_one_line(site,result)
        except Exception as e:
            print('got err ',e)
            with open ('graph_err.log','a') as f:
                f.write(str(result) + '\n' + str(e)+'\n'+'-----'+'\n')
    graph_db.update_contacts_from_sql('', results)


def update_countries_and_religions():
    conn=database.connect_db()
    cursor=conn.cursor()
    sql="select chats.index,text from chats"
    cursor.execute(sql)
    results=cursor.fetchall()
    for res in results:
        text=res['text']
        index=res['index']
        print(index)
        religions=extract.get_text_religions(text)
        countries=extract.get_text_countries(text)
        if len(religions)==0 and len(countries)==0:
            continue
        else:
            if len(religions)>0 and len(countries)>0:
                sql="update chats set religions='%s', countries='%s' where chats.index=%s "% (religions,countries,index)
            elif len(countries)>0:
                sql="update chats set countries='%s' where chats.index=%s "% (countries,index)
            else:
                sql="update chats set religions='%s' where chats.index=%s "% (religions,index)
            print(index,sql)
            cursor.execute(sql)
            conn.commit()
    conn.close()

def update_langauges():
    translator=Translator()
    conn=database.connect_db()
    cursor=conn.cursor()
    sql="select chats.index,text from chats where chats.index > 219026"
    cursor.execute(sql)
    results=cursor.fetchall()
    for res in results:
        text=res['text']
        index=res['index']
        detection=translator.detect(text)
        lang=detection.lang
        conf=detection.confidence
        if conf>.90:
            sql_query="update chats set language='%s' where chats.index=%s"
            vals=(lang,index)
            sql=sql_query%vals
            print(sql)
            cursor.execute(sql)
            conn.commit()


def pycountry_test():
    country_dict=bot_config.country_dict
    print(country_dict)
    # for i in range (len(pycountry.countries)):
    #     #print (i,list(pycountry.countries)[i])
    #     country_details=list(pycountry.countries)[i]
    #     country_dict[country_details.name]=1
    # res=pycountry.countries.search_fuzzy('-')
    # print(res)
    # print(type(res[0].name))

def get_all_countries_and_religions():
    all_countries=[]
    all_religions=[]
    conn=database.connect_db()
    cursor=conn.cursor()
    sql="select countries,religions from chats"
    cursor.execute(sql)
    results=cursor.fetchall()
    for res in results:
        countries=res['countries'].split(',')
        all_countries+=countries
        if res['religions'] is None:
            continue
        religions=res['religions'].split(',')
        all_religions+=religions
    all_countries=list(set(all_countries))
    all_countries.remove('')
    all_religions=list(set(all_religions))
    all_religions.remove('')
    print(all_countries,all_religions)
    return all_countries,all_religions


def create_religion_and_country_nodes_in_graph():
    all_countries,all_religions=get_all_countries_and_religions()
    graph=graph_db.get_graph()
    for religion in all_religions:
        cypher="""merge (r:Religion{name:'%s'})""" %religion
        graph.run(cypher)
    for country in all_countries:
        cypher="""merge (r:Country{name:'%s'})""" %country
        graph.run(cypher)


def create_relationships_for_countries_and_religions():
    graph=graph_db.get_graph()
    cypher="""match (u:User)-[:Said]-(c:Chat) return u,c """
    results=graph.run(cypher).data()
    i=1
    for res in results:
        print(i,'/',len(results))
        i+=1
        #print(res)
        chat=res['c']['content']
        chat_id=res['c']['chat_id'].replace('"','\\"').replace("'","\\'")
        user=res['u']['user_name']
        countries=extract.get_text_countries(chat.lower())
        if len(countries)>0:
            all_countries=countries.split(',')
            for country in all_countries:
                if len(country)<1:
                    continue
                cypher_query=""" match (u:User{user_name:"%s"}) match (c:Chat{chat_id:"%s"}) match (country:Country{name:"%s"}) 
                        merge (u)-[:Mentioned]-(country) merge (c)-[:Labeled]-(country) """
                vals=(user,chat_id,country)
                cypher=cypher_query %vals
                print('country cypher: ',cypher)
                graph.run(cypher)
        religions=extract.get_text_religions(chat.lower())
        if len(religions)>0:
            all_religions=religions.split(',')
            for religion in all_religions:
                if len(religion)<1:
                    continue
                cypher_query=""" match (u:User{user_name:"%s"}) match (c:Chat{chat_id:"%s"}) match (religion:Religion{name:"%s"}) 
                        merge (u)-[:Mentioned]-(religion) merge (c)-[:Labeled]-(religion) """
                vals=(user,chat_id,religion)
                cypher=cypher_query % vals
                print('religion cypher: ', cypher)
                graph.run(cypher)


def test_lang_detection():
    translator=Translator()
    conn=database.connect_db()
    cursor=conn.cursor()
    counter=Counter()
    sql="select chats.index, text from chats where chats.index > 196198 order by chats.index desc"
    cursor.execute(sql)
    results=cursor.fetchall()
    foriegn_langs={}
    i=1
    failed=0
    for res in results:
        print(i,'/',len(results))
        i+=1
        text=res['text']
        index=res['index']
        line=TextBlob(text)
        print(line)
        try:
            # lang=line.detect_language()
            lang=translator.detect(text)
            print(lang)
            counter[lang]+=1
    #         if lang not in foriegn_langs.keys():
    #             foriegn_langs[lang]=[text]
    #         else:
    #             foriegn_langs[lang].append(text)
        except Exception as e:
            print('failed on ',len(line),e)
            failed+=1
    #     if i % 10 ==0:
    #         with open('text_blob_langs.json', 'w') as f:
    #             json.dump(foriegn_langs,f)
    # print('total failed: ',failed)
    # with open ('text_blob_langs.json','w') as f:
    #     json.dump(foriegn_langs,f)

def create_langs_table():
    langs_table={}
    with open ('langs_table.html') as f:
        table_html=f.read()
    soup=BeautifulSoup(table_html,'html.parser')
    rows=soup.select('tr')
    for row in rows:
        cells=row.select('td')
        code=BeautifulSoup(str((cells[1])),'html.parser').select('code')[0].text
        #code=code_soup.text
        lang=cells[0].text
        langs_table[code]=lang
        print(code)
    with open ('langs_table.json','w') as f:
        json.dump(langs_table,f)
    #print(soup)



if __name__=="__main__":
    graph_db.update_chats_language()

    #create_langs_table()
    # start_program()
    # create_religion_and_country_nodes_in_graph()
    # create_relationships_for_countries_and_religions()