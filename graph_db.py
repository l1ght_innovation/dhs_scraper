from py2neo import Graph
import pandas as pd
import ast
import tldextract
from collections import Counter
import urllib
import pprint
import extract
import sys
from googletrans import Translator




labels = ['album', 'animal', 'baby', 'boy', 'buy', 'cam', 'dad', 'drugs', 'extreme', 'felixserver',
          'genitals', 'girl', 'hebe', 'matrixserver', 'mom', 'parent', 'pedo', 'pedomomserver', 'pics', 'rape', 'sell',
          'toddler', 'video','gang','oral'
          ]

def get_graph():
    graph = Graph("bolt://localhost:7687", password='dhsPassword')
    return graph


def add_labels():
    graph = get_graph()
    for label in labels:
        cypher='Create (:Label{type:"%s"})'
        print(cypher%label)
        graph.run(cypher%label)

def init_graph():
    graph=get_graph()
    df=pd.read_csv('recent_chats.csv')
    for index,row in df.iterrows():
        try:
            site=row['site']
            text=row['text'].replace("'","\\'").replace('"','\\"')
            user_name=row['user_name']
            user_id=row['userid']
            date=str(row['date'])
            print('init',index,'/',len(df))
            if not user_id:
                user_id='unavailable'
            cypher_query="MERGE (u:User{user_name:'%s','user_id':'%s'}) MERGE (c:Chat{content:'%s',date:'%s'}) MERGE (s:Site{site:'%s'}) MERGE (u)-[:Said]->(c) MERGE (c)-[:In_room]->(s) merge (u)-[:Visited]-(s) "
            vals=(user_name,user_id,text,date,site)
            cypher=cypher_query % vals
            graph.run(cypher)
            for label in labels:
                if row[label]==1:
                    cypher_query=''
                    cypher_query+="match (c:Chat{content:'%s'}) " % text
                    cypher_query+="match (u:User{user_name:'%s'}) " % user_name
                    cypher_query+="merge (l:Label{type:'%s'}) " % label
                    cypher_query+="merge (c)-[:Labeled]-(l) "
                    cypher_query+="merge (u)-[:Mentioned]->(l) "
                    graph.run(cypher_query)
            images = ast.literal_eval(row['image_links'])
            for image in images:
                cypher_query=''
                cypher_query+="match (c:Chat{content:'%s'}) " % text
                cypher_query += "match (u:User{user_name:'%s'}) " % user_name
                cypher_query+="merge (i:Image{href:'%s'}) " %image
                cypher_query+="merge (c)-[:Has_Image]-(i) "
                cypher_query+="merge (u)-[:Shared_Image]->(i) "
                graph.run(cypher_query)
        except Exception as e:
            with open ('err.log','a+') as f:
                f.write(str(index)+'---'+str(e)+'\n')
                print('error:',index,e)

def add_images():
    graph=get_graph()
    df=pd.read_csv('recent_chats.csv')
    for index,row in df.iterrows():
        try:
            images=ast.literal_eval(row['image_links'])
            for image in images:
                text=row['text'].replace("'","\\'").replace('"','\\"')
                cypher_query="MERGE (t:Text{content:'%s'}) MERGE (i:Image{href:'%s'}) MERGE (t)-[:Has_Image]-(i)"
                cypher=cypher_query%(text,image)
                graph.run(cypher)
                print (index)
        except Exception as e:
            with open ('err2.log','a+') as f:
                f.write(str(index)+'---'+str(e)+'\n')
                print('err:',e)



def get_top_10_users_of_each_room():
    graph=get_graph()
    cypher="Match (site:Site) return site"
    sites=graph.run(cypher).data()
    results=[]
    for site in sites:
        site_name=site['site']['site']
        cypher="Match path=(u:User)-[:Said]-(t:Text)-[:In_room]-(s:Site{site:'%s'}) return u,s,count(path) as count order by count desc limit 10"% site_name
        res=graph.run(cypher).data()
        results+=(res)
    print(results)
    print(len(results))


def get_image_hosts():
    counter=Counter()
    graph=get_graph()
    cypher="match (i:Image) return i.href"
    results=graph.run(cypher).data()
    print(len(results))
    for res in results:
        href=res['i.href']
        if 'redirect&url' in href:
            index=href.find('redirect&url=')
            href=href[index+len('redirect&url')+1:]
            href=urllib.parse.unquote(href)
        host=tldextract.extract(href).fqdn
        counter[host]+=1
    pprint.pprint(counter.most_common())


def insert_contact():
    graph=get_graph()
    df=pd.read_csv('recent_chats.csv')
    for index,row in df.iterrows():
        print('contacts',index)
        if isinstance(row['known_contacts'],float):
            known_contacts=''
        else:
            known_contacts=row['known_contacts'].split(',')
        if isinstance(row['unknown_contacts'],float):
            unknown_contact=row['unknown_contacts']=''
        else:
            unknown_contact=row['unknown_contacts'].split(',')
        u1=row['user_name']
        for contact in known_contacts:
            cypher_query="""merge (u1:User{user_name:'%s'}) merge (u2:User{user_name:'%s'}) merge (u1)-[:Talked_To]->(u2) """
            vals=(u1,contact)
            cypher=cypher_query % vals
            graph.run(cypher)
        for contact in unknown_contact:
            cypher_query="""merge (u1:User{user_name:"%s"}) merge (u2:Unknow_User{user_name:"%s"}) merge (u1)-[:Talked_To]->(u2) """
            vals=(u1,contact)
            cypher=cypher_query % vals
            graph.run(cypher)


def insert_one_line(chat_name,result):
    #print('working on this results: ',result)
    graph=get_graph()
    site=chat_name
    user_name=result['user_name'].replace('"','\\"').replace("'","\\'")
    chat=result['text'][:500].replace('"','\\"').replace("'","\\'")
    date=result['date'][:result['date'].rfind('-')]
    time=result['date'][result['date'].rfind('-')+1:]
    lang=result['language']
    #print('lang= ',lang)
    chat_id=site+'_'+date+'_'+chat
    cypher_query = """MERGE (u:User{user_name:'%s'}) MERGE (c:Chat{content:'%s',date:datetime('%sT%s'),chat_id:'%s',language:'%s'}) MERGE (s:Site{site:'%s'}) MERGE (u)-[:Said]->(c) MERGE (c)-[:In_room]->(s) MERGE (u)-[:Visited]-(s) """
    vals = (user_name, chat, date,time,chat_id,lang, site)
    cypher = cypher_query % vals
    #print('cypher: ',cypher)
    graph.run(cypher)
    for label in labels:
        if result[label] == 1:
            cypher_query = ''
            cypher_query += "match (c:Chat{chat_id:'%s'}) " % chat_id
            cypher_query += "match (u:User{user_name:'%s'}) " % user_name
            cypher_query += "merge (l:Label{type:'%s'}) " % label
            cypher_query += "merge (c)-[:Labeled]->(l) "
            cypher_query += "merge (u)-[:Mentioned]->(l) "
            graph.run(cypher_query)
    try:
        images = ast.literal_eval(result['image_links'])
    except:
        return
    if not isinstance(images,list):
        images=(images,)
    for image in images:
        cypher_query = ''
        cypher_query += "match (c:Chat{chat_id:'%s'}) " % chat_id
        cypher_query += "match (u:User{user_name:'%s'}) " % user_name
        cypher_query += "merge (i:Image{href:'%s'}) " % image
        cypher_query += "merge (c)-[:Has_Image]-(i) "
        cypher_query += "merge (u)-[:Shared_Image]->(i) "
        graph.run(cypher_query)
    countries=result['countries']
    if len(countries)>0:
        all_countries=countries.split(',')
        for country in all_countries:
            if len(country)<1:
                continue
            else:
                cypher_query=''
                cypher_query += "match (c:Chat{chat_id:'%s'}) " % chat_id
                cypher_query += "match (u:User{user_name:'%s'}) " % user_name
                cypher_query += "match (country:Country{name:'%s'}) " % country
                cypher_query += "merge (c)-[:Labeled]->(country)"
                cypher_query += "merge (u)-[:Mentioned]-(country) "
                graph.run(cypher_query)
    religions=result['religions']
    if len(religions)>0:
        all_religions=religions.split(',')
        for religion in all_religions:
            if len(religion)<1:
                continue
            else:
                cypher_query=''
                cypher_query += "match (c:Chat{chat_id:'%s'}) " % chat_id
                cypher_query += "match (u:User{user_name:'%s'}) " % user_name
                cypher_query += "match (r:Religion{name:'%s'}) " % religion
                cypher_query += "merge (c)-[:Labeled]->(r)"
                cypher_query += "merge (u)-[:Mentioned]-(r) "
                graph.run(cypher_query)
    return


def get_user_dict_from_graph():
    graph=get_graph()
    cypher="MATCH (u:User) RETURN (u)"
    users=graph.run(cypher).data()
    user_dict={u['u']['user_name'] for u in users}
    user_dict=dict.fromkeys(user_dict,1)
    return user_dict

def update_contacts_from_scrape(site,results):
    graph = get_graph()
    all_known_users=get_user_dict_from_graph()
    i=1
    total=len(results)
    print('\n')
    for result in results:
        #site=result['site']
        sys.stdout.write ('\r updating contacts in graph database %d / %d' % (i,total))
        i+=1
        chat=result['text'][:500].replace("'","\\'").replace('"','\\"')
        known_contacts,unknown_contacts=extract.get_contacts_from_text(chat,all_known_users)
        if isinstance(known_contacts,str)  and known_contacts!='':
            known_contacts=(known_contacts,)
        if isinstance(unknown_contacts,str) and unknown_contacts!='':
            unknown_contacts=(unknown_contacts,)
        date = result['date'][:result['date'].rfind('-')]
        chat_id=site+'_'+date+'_'+chat
        user=result['user_name'].replace('"','\\"').replace("'","\\'")
        for contact in known_contacts:
            cypher_query="""merge (u1:User{user_name:'%s'}) merge (u2:User{user_name:'%s'}) merge (c:Chat{chat_id:'%s'}) merge (u1)-[:Talked_To]->(u2) merge (u2)-[:Is_Mentioned_In]-(c) """
            vals=(user,contact,chat_id)
            cypher=cypher_query % vals
            try:
                graph.run(cypher)
            except Exception as e:
                with open ('graph_err.log','a') as f:
                    print(e)
                    f.write(str(result) + '\n' + str(e)+'\n'+'-----'+'\n')
                continue
        for contact in unknown_contacts:
            cypher_query="""merge (u1:User{user_name:"%s"}) merge (u2:Unknown_User{user_name:"%s"}) merge (c:Chat{chat_id:'%s'}) merge (u1)-[:Talked_To]->(u2) merge (u2)-[:Is_Mentioned_In]-(c) """
            vals=(user,contact,chat_id)
            cypher=cypher_query % vals
            try:
                graph.run(cypher)
            except Exception as e:
                print(e)
                with open('graph_err.log', 'a') as f:
                    f.write(str(result) + '\n' + str(e) + '\n' + '-----' + '\n')
                continue
    print('\n')


def update_contacts_from_sql(site,results):
    graph = get_graph()
    all_known_users=get_user_dict_from_graph()
    i=0
    for result in results:
        site=result['site']
        print('contacts',i,'/',len(results))
        i+=1
        chat=result['text'][:500].replace("'","\\'").replace('"','\\"')
        known_contacts,unknown_contacts=extract.get_contacts_from_text(chat,all_known_users)
        if isinstance(known_contacts,str)  and known_contacts!='':
            known_contacts=(known_contacts,)
        if isinstance(unknown_contacts,str) and unknown_contacts!='':
            unknown_contacts=(unknown_contacts,)
        date = result['date'][:result['date'].rfind('-')]
        chat_id=site+'_'+date+'_'+chat
        user=result['user_name'].replace('"','\\"').replace("'","\\'")
        for contact in known_contacts:
            cypher_query="""merge (u1:User{user_name:'%s'}) merge (u2:User{user_name:'%s'}) merge (c:Chat{chat_id:'%s'}) merge (u1)-[:Talked_To]->(u2) merge (u2)-[:Is_Mentioned_In]-(c) """
            vals=(user,contact,chat_id)
            cypher=cypher_query % vals
            try:
                graph.run(cypher)
            except Exception as e:
                print(e)
                with open ('graph_err.log','a') as f:
                    f.write(str(result) + '\n' + str(e)+'\n'+'-----'+'\n')
                continue
        for contact in unknown_contacts:
            cypher_query="""merge (u1:User{user_name:"%s"}) merge (u2:Unknown_User{user_name:"%s"}) merge (c:Chat{chat_id:'%s'}) merge (u1)-[:Talked_To]->(u2) merge (u2)-[:Is_Mentioned_In]-(c) """
            vals=(user,contact,chat_id)
            cypher=cypher_query % vals
            #print(cypher)
            try:
                graph.run(cypher)
            except Exception as e:
                print(e)
                with open('graph_err.log', 'a') as f:
                    f.write(str(result) + '\n' + str(e) + '\n' + '-----' + '\n')
                continue


def fix_unknown_user(unknown_user,known_user):
    graph=get_graph()
    cypher="""MATCH (u:User)-[:Talked_To]->(u2:Unknown_User{user_name:'%s'})-[:Is_Mentioned_In]->(c:Chat) RETURN u.user_name,u2.user_name,c.chat_id""" % unknown_user
    results=graph.run(cypher).data()
    print('A: ' , results)
    cypher="""MATCH (u:User{user_name:'%s'}) RETURN u.user_name""" % known_user
    known_user_node=graph.run(cypher).data()
    print('B : ', known_user_node)
    for res in results:
        cypher_query="""MATCH (u1:User{user_name:'%s'}) MATCH (u4:User{user_name:'%s'}) MATCH (c:Chat{chat_id:'%s'}) MATCH (u3:Unknown_User{user_name:'%s'})
                        MERGE (u1)-[:Talked_to]->(u4)-[:Is_Mentioned_In]->(c) MERGE (u3)-[:Is_Alias_For]-(u4)"""
        vals=(res['u.user_name'], known_user_node[0]['u.user_name'],res['c.chat_id'],res['u2.user_name'])
        cypher=cypher_query % vals
        print(cypher)
        graph.run(cypher)


def fix_unknown_users_with_dict(user_dict):
    #to use this method you must suplly a dict where the key is the unknown user, and the value is the user the key refers to
    for alias,user in user_dict.items():
        fix_unknown_user(alias,user)

def update_chats_language():
    translator=Translator()
    graph=get_graph()
    cypher="match (c:Chat) where not exists(c.language) return c"
    results=graph.run(cypher).data()
    total=len(results)
    i=1
    for res in results:
        try:
            print(i,'/',total, res)
            i+=1
            language=''
            text=res['c']['content']
            chat_id=res['c']['chat_id'].replace('"','\\"').replace("'","\\'")
            # if len(text)>1:
            #     continue
            detection=translator.detect(text)
            lang=detection.lang
            conf=detection.confidence
            if conf>.90:
                language=lang
            cypher= """MATCH (c:Chat) where c.chat_id='%s' set c.language='%s'"""
            cypher=cypher %(chat_id,language)
            print(cypher)
            graph.run(cypher)
        except Exception as e:
            with open ('graph_lang_err.log','w') as f:
                f.write(str(res)+'.'+str(e)+'\n')
