import pycountry
import json

paths={
    "BoysLand":{
        "chat_frame_xpath":"//frame[@name='view']",
        "post_frame_xpath":"//frame[@name='post']",
        "input_selector" : "//div[@class='post_message']/input",
        "send_selector" : "//input[@type='submit']",
        "start_index":0,
        "message_path":"msg",
        "captha_field": "captcha",
        "captcha_img_wrapper":"tr#captcha"
    },
    "BoysPub":{
        "chat_frame_xpath": "//frame[@name='view']",
        "post_frame_xpath": "//frame[@name='post']",
        "input_selector": "//input[@name='message']",
        "send_selector": "//input[@type='submit']",
        "start_index":0,
        "message_path":"msg",
        "captha_field": "captcha",
        "captcha_img_wrapper":"tr#captcha"
    },
    "The Annex":{
        "chat_frame_xpath": "//frameset[1]/frame[3]",
        "post_frame_xpath": "//frameset[1]/frame[1]",
        "input_selector": "//input[@name='m']",
        "send_selector": "//input[@value='Send']",
        "start_index": -1,
        "message_path": ".messages div"
    },
    "TorPedoChat": {
        "chat_frame_xpath": None,
        "post_frame_xpath": "//iframe[@id='menu']",
        "input_selector": "//input[@name='m']",
        "send_selector": "//input[@type='submit']",
        "start_index": -1,
        "message_path": "#messages p",
        "captcha_field": "cr",
        "captcha_img_wrapper":"div#login"
    },
    "The Public Pedo Club": {
        "chat_frame_xpath": "//frame[@name='view']",
        "post_frame_xpath": "//frame[@name='post']",
        "input_selector": "//input[@name='message']",
        "send_selector": "//input[@type='submit']",
        "start_index": 0,
        "message_path": "msg",
        "captha_field": "oooopsfive",
        "captcha_img_wrapper":"div.oooopszoom"
    },
    "LoliPub": {
        "chat_frame_xpath": "//frame[@name='view']",
        "post_frame_xpath": "//frame[@name='post']",
        "input_selector": "//input[@name='message']",
        "send_selector": "//input[@type='submit']",
        "start_index": 0,
        "message_path": "msg",
        "captha_field": "captcha",
        "captcha_img_wrapper":"tr#captcha"
    },
    "Toddlers Amuse": {
        "chat_frame_xpath": "//frame[@name='view']",
        "post_frame_xpath": "//frame[@name='post']",
        "input_selector": "//input[@name='message']",
        "send_selector": "//input[@type='submit']",
        "start_index": 0,
        "message_path": "msg",
        "captha_field": "captcha",
        "captcha_img_wrapper":""
    },
}
pic_response=['great pic','so good!','awesome!', 'luv it!','gets my juices flowing','yummm','yummy!']
thank_yous=['thanks posters, you rock!', 'thank u all posters for the good stuff','thanks so much to all the posters!','thank you posters and chatters for keepin the board alive!','all hail the posters :)']
greetings=['hello all','hi all','hi guys']
low_chat=['any dads in the house?', 'anyone travel to asia?','any cute boys around?']
known_religions='christian,mormon,protestant,catholic,buddhist,hindu,antheist,jew,arab,muslim,anglican,adventist,sikh'.split(',')

def create_country_dict():
    countries_list=pycountry.countries
    countries_dict={country.name.lower():1 for country in list(countries_list)}
    return countries_dict

country_dict=create_country_dict()

with open('langs_table.json') as f:
    langs_table=json.load(f)