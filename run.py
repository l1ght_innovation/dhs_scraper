import main
import database
import extract
import pandas as pd
import json
import traceback,sys
import chat_bot
from pick import pick
from collections import Counter
import graph_db
import datetime
import pprint





def test():
    print('testing....')
    sites = database.get_sites()
    for site in sites:
        print(site['name'])
        if site['name'] in ["TorPedoChat","BoysPub","The Annex"]:
            try:
                driver=main.general_login(site)
                print('this is the driver:',driver)
                site['driver']=driver
                results=main.general_extract(driver,site)
                try:
                    driver.close()
                except:
                    print('no driver to close')
                print(results)
                df=pd.DataFrame(results)
                df.to_csv('{}_test.csv'.format(site['name']),index=False)
            except Exception as e:
                traceback.print_exc(file=sys.stdout)
                print('failed on ',site['name'],e)
                continue
        else:
            continue



def start_chatbot(site):
    driver=main.general_login(site)
    results=[]
    try:
        chat_bot.start_bot(driver,results,site)
    except Exception as e:
        print('failed in start_chatbot',e)
        traceback.print_exc(file=sys.stdout)
        chat_bot.start_bot(driver,results,site)


def run_chatbot():
    sites = database.get_sites()
    options=[]
    for site in sites:
        if site['type'] in [1,2,3,4,5]:
            options.append(site['name'])
    title="Please select site to scrape. Scroll with arrow keys, enter to select "
    selected = pick(options,title, min_selection_count=1)
    picked_site=[site for site in sites if site['name'] == selected[0]][0]
    start_chatbot(picked_site)

def run_scraper():
    sites = database.get_sites()
    options=['all']
    sites_to_scrape=[]
    picked_sites=[]
    #results_dict={}
    for site in sites:
        if site['type'] in [1,2,3,4,5]:
            options.append(site['name'])
    title="Please select sites to scrape. Scroll with arrow keys, select with spacebar, enter to submit "
    selected = pick(options, title, multiselect=True, min_selection_count=1)
    if selected[0][0]=='all':
        sites_to_scrape=[site for site in sites if site['type'] in [1,2,3,4,5]]
    else:
        for selection in selected:
            picked_sites.append(selection[0])
        for site in sites:
            if site['name'] in picked_sites:
                sites_to_scrape.append(site)
    results_dict=dict.fromkeys((site['name'] for site in sites_to_scrape),0)
    for site in sites_to_scrape:
        attempts=0
        while results_dict[site['name']]==0 and attempts<3:
            attempts+=1
            try:
                driver = main.general_login(site)
                site['driver'] = driver
                results = main.general_extract(driver, site)
                results_dict[site['name']]=len(results)
                if results_dict[site['name']]==0:
                    driver = main.general_login(site)
                    site['driver'] = driver
                    results = main.general_extract(driver, site)
                    results_dict[site['name']] = len(results)
                try:
                    driver.close()
                except:
                    print('no driver to close')
            except Exception as e:
                traceback.print_exc(file=sys.stdout)
                print('failed on ', site['name'], e)
                continue
    print('finished with these results: ')
    pprint.pprint(results_dict,indent=2)

def test_contact_extraction():
    counter=Counter()
    conn=database.connect_db()
    cursor=conn.cursor()
    sql="select text from chats"
    cursor.execute(sql)
    results=cursor.fetchall()
    i=0
    total=len(results)
    for res in results:
        i+=1
        contacts=extract.get_contacts_from_text(res['text'])
        if len(contacts)>0:
            counter[contacts]+=1
    with open ('test_contacts.json','w') as f:
        f.write(json.dumps(counter,indent=2))
    print(len(counter))


def start_program():
    title = "Please select in-and-out extraction, or keep-alive chatbot connection"
    options=['in and out!','keep-alive chatbot']
    selected = pick(options, title, min_selection_count=1)
    if selected[1]==0:
        run_scraper()
    elif selected[1]==1:
        run_chatbot()
    else:
        print('could not make selection')
        return


def init_graph_from_sql():
    conn=database.connect_db()
    cursor=conn.cursor()
    sql="select * from chats where date >= '2020-08-01' "
    cursor.execute(sql)
    results=cursor.fetchall()
    print(len(results))
    i=0
    for result in results:
        print(i,'/',len(results))
        i+=1
        result['date']=datetime.datetime.strftime(result['date'],'%Y-%m-%d-%H:%M:%S')[:-3]
        site=result['site']
        try:
            graph_db.insert_one_line(site,result)
        except Exception as e:
            print('got err ',e)
            with open ('graph_err.log','a') as f:
                f.write(str(result) + '\n' + str(e)+'\n'+'-----'+'\n')
    graph_db.update_contacts_from_sql('', results)



if __name__=="__main__":
    # start_program()
    run_scraper()
